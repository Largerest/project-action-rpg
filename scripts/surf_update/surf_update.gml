/// @description surface_update(surf,width,height)
/// @param surf
/// @param width
/// @param height
if !surface_exists(surf[? argument0]){
    surf[? argument0] = surface_create(argument1,argument2);
}
