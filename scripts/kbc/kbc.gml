///@arg key
///@arg check_type - optional

var key = is_string(argument[0])? ord(argument[0]): argument[0];
var ret = [
	keyboard_check(key),
	keyboard_check_pressed(key),
	keyboard_check_released(key)
];

if argument_count > 1 return ret[argument[1]];
return ret;