/// @description dif(val1,val2,mul)
/// @param val1
/// @param val2
/// @param mul
var val1 = argument0;
var val2 = argument1;
var mul = argument2;

if val1 > val2{
    return (val1 - val2)*mul;
}else{
    return (val2 - val1)*mul;
}
