/// @description scr_obj_collide()
if (collide){

    //This is for inter-pixel movement
    cx += h;
    cy += v;
    var hNew = floor(cx);
    var vNew = floor(cy);
    cx -= hNew;
    cy -= vNew;
    
    //horisontal
    repeat(abs(hNew)){
        var sh = sign(hNew);
        if place_meeting(x+sh,y,obj_solid){
            h *= -bounce;
            break;
        }else{
            x += sh;
        }
    }
    //Vertical
    repeat(abs(vNew)){
        var sv = sign(vNew);
        if place_meeting(x,y+sv,obj_solid){
            v *= -bounce;
            break;
        }else{
            y += sv;
        }
    }
}else{
//If this object doesn't collide, then just move
    x += h;
    y += v;
}
