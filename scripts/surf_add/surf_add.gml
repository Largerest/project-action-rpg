/// @description surface_add(surf,width,height)
/// @param surf
/// @param width
/// @param height
surf[? argument0] = surface_create(argument1,argument2);
