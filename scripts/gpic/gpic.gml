/// @description gpic(controller,button,type,axis)
/// @param controller
/// @param button
/// @param type
/// @param axis

var con = argument0;
var but = argument1;
var type = argument2;
var axi = argument3;

if but != "N/A"{
    switch type{
        case 0:
            return gamepad_button_check(con,but);
        break;
        
        case 1:
            return gamepad_button_check_pressed(con,but);
        break;
        
        case 2:
            return gamepad_button_check_released(con,but);
        break;
    }
}else{
    return gamepad_axis_value(con,axi);
}
