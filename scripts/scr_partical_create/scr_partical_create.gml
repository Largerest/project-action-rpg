/// @description scr_partical_create(system number,preset)
/// @param system number
/// @param preset
part[argument0] = instance_create(x,y,obj_partical);
part[argument0].owner = id;
with part[argument0]{
    preset = argument1;
    
    switch preset{
        case "Walking Dust":
        ep_max = 10;
        ep_uds = 2;
        active = 0;
        
        ep_spr = ef_dust;
        ep_life = 4;
        ep_bpx = 0;
        ep_bpy = 0;
        ep_prh = 2;
        ep_prv = 2;
        ep_mns = .25;
        ep_mxs = .75;
        ep_mnd = 45;
        ep_mxd = 90+45;
        ep_mnsi = 1;
        ep_mxsi = 2;
        ep_mnr = 0;
        ep_mxr = 0;
        break;
        
        case "Shadow Aura":
        ep_max = 20;
        ep_uds = 2;
        active = 1;
        
        ep_spr = ef_dark_flame;
        ep_life = 4;
        ep_bpx = 0;
        ep_bpy = -8;
        ep_prh = 8;
        ep_prv = 8;
        ep_mns = 0.1;
        ep_mxs = 0.25;
        ep_mnd = 0;
        ep_mxd = 359;
        ep_mnsi = 1;
        ep_mxsi = 2;
        ep_mnr = 0;
        ep_mxr = 0;
        break;
    }
    
    a[0] = -1;
    for (var e = 0;e<=ep_max;e++){
        if active{
            ep[e,A] = 0;
            ep[e,X] = x+ep_bpx+irandom_range(-ep_prh,ep_prh);
            ep[e,Y] = y+ep_bpy+irandom_range(-ep_prv,ep_prv);
            ep[e,S] = random_range(ep_mns,ep_mxs);
            ep[e,D] = irandom_range(ep_mnd,ep_mxd);
            ep[e,Si] = random_range(ep_mnsi,ep_mxsi);
            ep[e,R] = irandom_range(ep_mnr,ep_mxr);
            
            if e = 0{
                ep[e,A] = 0;
                a[0] = ep_uds;
            }
        }else{
            ep[e,A] = 0;
            ep[e,X] = 0;
            ep[e,Y] = 0;
            ep[e,S] = 0;
            ep[e,D] = 0;
            ep[e,Si] = 0;
            ep[e,R] = 0;
        }
    }
    i = 0;
}
