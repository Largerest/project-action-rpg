/// @description gamepad_debug_message
/*
** Script by TheXtraTecnologies
**
** This could be used to check which gamepad(s) is/are connected.
** (and their description)
**
** description might look like this:
**   - PLAYSTATION(R)3 Controller
**   - Xbox 360 Controller (XInput STANDARD GAMEPAD)
*/
gp_count = gamepad_get_device_count();
for(var i = 0; i < gp_count; i ++){
    if(gamepad_is_connected(i)){
        show_debug_message(gamepad_get_description(i) + " connected.");
    }else{
        show_debug_message("gamepad "+string(i)+" not connected");
    }
}
