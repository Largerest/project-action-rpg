/// @description draw_menu()
var c = c_white;

if pause{
    draw_set_center();
    draw_set_font(fnt_pixel_2);
    for (var i = 0;i<=width;i++){
        for (var e = 0;e<=height;e++){
            if menu_func[i,e] != btn_func_do_nothing{
                var xx = (game_width/2)+((-width/2+i)*(sprite_width+4));
                var yy = game_height/2;
                
                if cursor[_x] == i && cursor[_y] == e{
                    draw_sprite(menu_spr_2[i,e],sys_step*0.5,xx+irandom_range(-1,1),yy+irandom_range(-1,1));
                }else{
                    draw_sprite(menu_spr_1[i,e],sys_step*0.5,xx,yy);
                }
                draw_text(xx,yy,string_hash_to_newline(menu_txt[i,e]));
            }
        }
    }
    draw_text_colour(game_width/2,game_height/2-32,string_hash_to_newline(game_display_name),c,c,c,c,1);
    draw_reset_alignment();
    draw_reset_font();
}
