/// @description scr_obj_update()
depth = -y-z;
if !(pause || step_pause){
    alarm_manage();
    cl = place_meeting(x-1,y,obj_solid);
    cr = place_meeting(x+1,y,obj_solid);
    cu = place_meeting(x,y-1,obj_solid);
    cd = place_meeting(x,y+1,obj_solid);

    scr_obj_collide();
    scr_obj_animate();
    image_xscale = xscale*size;
    image_yscale = yscale*size;
    scr_obj_record_steps();
    z = round(z);
}else{
    scr_obj_playback();
}
