/// @description scr_obj_init()
image_speed = 0;
hspeed = 0;
vspeed = 0;
gravity = 0;
friction = 0;
///position/movement variables
h = 0;
v = 0;
cx = 0; //cx and cy are used for non-full pixel movement
cy = 0;
bms = 2;
sms = 2;
spd = 0;
dir = 0;
acc = 1;
dcc = 1;
tmpacc = dcc;
z = 0;
zspd = 0;
//collisions
cl = place_meeting(x-1,y,obj_solid);
cr = place_meeting(x+1,y,obj_solid);
cu = place_meeting(x,y-1,obj_solid);
cd = place_meeting(x,y+1,obj_solid);
///image and animation variables
cs = 0;
spr[0] = noone;
is = 0.35;
istop = -1;
pos = 0;
e = 0;
xscale = 1;
yscale = 1;
size = 1;
rot = 0;
///collide with blocks?
collide = true;
bounce = 0;
name = "";
name_alpha = 0;

alarm_create(10);

//playback
scr_obj_record_steps();
