/// @description debug(restart_button,end_button)
/// @param restart_button
/// @param end_button
if kbc(argument0,2){
    game_restart();
}
if kbc(argument1,2){
    game_end();
}
