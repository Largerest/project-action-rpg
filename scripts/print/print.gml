///@description print(str*,...)
///@param str*
///@param ...

var str = "";
for(var i = 0; i < argument_count;i++)
	str += string(argument[i])+" ";

var objID = string(id);
var objIndex = string(object_get_name(object_index));
show_debug_message("["+objIndex+": "+objID+"]"+" "+str);
