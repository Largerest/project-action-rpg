/// @description loop(val,min,max)
/// @param val
/// @param min
/// @param max
var va = argument0;
var mi = argument1;
var ma = argument2;

if va > ma{
    va = mi;
}else if va < mi{
    va = ma;
}
return va;
