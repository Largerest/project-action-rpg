/// @description track_object(view,obj,xoffset,yoffset,spd)
/// @param view
/// @param obj
/// @param xoffset
/// @param yoffset
/// @param spd
var view,obj,xx,yy,spd;
view = argument0;
obj = argument1;
xx = argument2;
yy = argument3;
spd = argument4;

__view_set( e__VW.XView, view, __view_get( e__VW.XView, view ) + ((((obj.x+xx)-(__view_get( e__VW.XView, view )+__view_get( e__VW.WView, view )/2))*spd)) );
__view_set( e__VW.YView, view, __view_get( e__VW.YView, view ) + ((((obj.y+yy)-(__view_get( e__VW.YView, view )+__view_get( e__VW.HView, view )/2))*spd)) );

__view_set( e__VW.XView, view, round(__view_get( e__VW.XView, view )) );
__view_set( e__VW.YView, view, round(__view_get( e__VW.YView, view )) );

if __view_get( e__VW.WView, view ) <= room_width && __view_get( e__VW.HView, view ) <= room_height{
    __view_set( e__VW.XView, view, clamp(__view_get( e__VW.XView, view ),0,room_width-__view_get( e__VW.WView, view )) );
    __view_set( e__VW.YView, view, clamp(__view_get( e__VW.YView, view ),0,room_height-__view_get( e__VW.HView, view )) );
}
