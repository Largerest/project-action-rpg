/// @description gamepad_get_first_connected()
/*
** Script by TheXtraTechnologies
**
** This script will return the first connected gamepad
*/
var gp_connected_count;

gp_connected_count = gamepad_get_device_count();

for(var i = 0; i < gp_connected_count; i++){
    if(gamepad_is_connected(i)){
        return i;
    }
}
