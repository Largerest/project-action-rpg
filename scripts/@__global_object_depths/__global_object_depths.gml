// Initialise the global array that allows the lookup of the depth of a given object
// GM2.0 does not have a depth on objects so on import from 1.x a global array is created
// NOTE: MacroExpansion is used to insert the array initialisation at import time
gml_pragma( "global", "__global_object_depths()");

// insert the generated arrays here
global.__objectDepths[0] = 0; // sys_dj
global.__objectDepths[1] = 0; // sys_menu
global.__objectDepths[2] = 0; // sys_time
global.__objectDepths[3] = -10000; // sys_control
global.__objectDepths[4] = 0; // sys_controls
global.__objectDepths[5] = 0; // obj_ai
global.__objectDepths[6] = 0; // obj_player
global.__objectDepths[7] = 0; // sys_layers
global.__objectDepths[8] = 0; // obj_obj
global.__objectDepths[9] = 0; // obj_dungeon_person
global.__objectDepths[10] = 0; // obj_ferrosia
global.__objectDepths[11] = 0; // track
global.__objectDepths[12] = 0; // obj_lili
global.__objectDepths[13] = 0; // obj_enemy
global.__objectDepths[14] = 0; // obj_slitherer
global.__objectDepths[15] = 0; // obj_shadow
global.__objectDepths[16] = 0; // obj_shadow_puddle
global.__objectDepths[17] = 0; // obj_item
global.__objectDepths[18] = 0; // obj_track_cleets
global.__objectDepths[19] = 0; // obj_ice_skates
global.__objectDepths[20] = 0; // obj_pot
global.__objectDepths[21] = 0; // obj_super_mushroom
global.__objectDepths[22] = 0; // obj_bullet
global.__objectDepths[23] = 0; // obj_coin
global.__objectDepths[24] = 0; // obj_solid
global.__objectDepths[25] = 0; // obj_flicker
global.__objectDepths[26] = 0; // obj_partical
global.__objectDepths[27] = 0; // obj_gfx
global.__objectDepths[28] = 0; // obj_house


global.__objectNames[0] = "sys_dj";
global.__objectNames[1] = "sys_menu";
global.__objectNames[2] = "sys_time";
global.__objectNames[3] = "sys_control";
global.__objectNames[4] = "sys_controls";
global.__objectNames[5] = "obj_ai";
global.__objectNames[6] = "obj_player";
global.__objectNames[7] = "sys_layers";
global.__objectNames[8] = "obj_obj";
global.__objectNames[9] = "obj_dungeon_person";
global.__objectNames[10] = "obj_ferrosia";
global.__objectNames[11] = "track";
global.__objectNames[12] = "obj_lili";
global.__objectNames[13] = "obj_enemy";
global.__objectNames[14] = "obj_slitherer";
global.__objectNames[15] = "obj_shadow";
global.__objectNames[16] = "obj_shadow_puddle";
global.__objectNames[17] = "obj_item";
global.__objectNames[18] = "obj_track_cleets";
global.__objectNames[19] = "obj_ice_skates";
global.__objectNames[20] = "obj_pot";
global.__objectNames[21] = "obj_super_mushroom";
global.__objectNames[22] = "obj_bullet";
global.__objectNames[23] = "obj_coin";
global.__objectNames[24] = "obj_solid";
global.__objectNames[25] = "obj_flicker";
global.__objectNames[26] = "obj_partical";
global.__objectNames[27] = "obj_gfx";
global.__objectNames[28] = "obj_house";


// create another array that has the correct entries
var len = array_length_1d(global.__objectDepths);
global.__objectID2Depth = [];
for( var i=0; i<len; ++i ) {
	var objID = asset_get_index( global.__objectNames[i] );
	if (objID >= 0) {
		global.__objectID2Depth[ objID ] = global.__objectDepths[i];
	} // end if
} // end for