/// @description track_coord(view,xx,yy,spd)
/// @param view
/// @param xx
/// @param yy
/// @param spd
var view,xx,yy,spd;
view = argument0;
xx = argument1;
yy = argument2
spd = argument3;

__view_set( e__VW.XView, view, __view_get( e__VW.XView, view ) + ((xx-(__view_get( e__VW.XView, view )+__view_get( e__VW.WView, view )/2))*spd) );
__view_set( e__VW.YView, view, __view_get( e__VW.YView, view ) + ((yy-(__view_get( e__VW.YView, view )+__view_get( e__VW.HView, view )/2))*spd) );

__view_set( e__VW.XView, view, clamp(__view_get( e__VW.XView, view ),0,room_width-__view_get( e__VW.WView, view )) );
__view_set( e__VW.YView, view, clamp(__view_get( e__VW.YView, view ),0,room_height-__view_get( e__VW.HView, view )) );
