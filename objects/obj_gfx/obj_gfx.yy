{
    "id": "8e506ffb-78c7-4525-a8de-f8f88fbacaca",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_gfx",
    "eventList": [
        {
            "id": "10518957-10bc-4cc2-b77c-58795ff2f629",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8e506ffb-78c7-4525-a8de-f8f88fbacaca"
        },
        {
            "id": "cc442836-50ea-4602-a334-ec0c2587239e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "8e506ffb-78c7-4525-a8de-f8f88fbacaca"
        },
        {
            "id": "2c7a8f11-0cb5-4972-b3e7-97c34b00c1ce",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "8e506ffb-78c7-4525-a8de-f8f88fbacaca"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "6ee4a807-06a4-4bb1-befb-2bcb9093598a",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}