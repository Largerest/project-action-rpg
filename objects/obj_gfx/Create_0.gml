action_inherited();
spr[cs] = gfx_explosion;
audio_play_sound(snd_gun,0,0);
rot = irandom(360);

intencity = 6;

__view_set( e__VW.XView, 0, __view_get( e__VW.XView, 0 ) + (irandom_range(-intencity,intencity)) );
__view_set( e__VW.YView, 0, __view_get( e__VW.YView, 0 ) + (irandom_range(-intencity,intencity)) );

