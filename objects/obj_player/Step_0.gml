event_inherited();
with controlling{
	my_bleft    = other.bleft;
	my_bright   = other.bright;
	my_bup      = other.bup;
	my_bdown    = other.bdown;
	my_bact1    = other.bact1;
	my_bact     = other.bact2;
	my_bact3    = other.bact3;
	my_bmenu    = bmenu;

	my_bah	= other.bah;
	my_bav	= other.bav;
	my_bdir = other.bdir;
}	