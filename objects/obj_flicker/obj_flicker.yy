{
    "id": "1d5b9b6e-5730-411e-9285-ce4be825cd5c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_flicker",
    "eventList": [
        {
            "id": "e06419ec-8ea3-4b91-b450-ee95f36ca995",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "1d5b9b6e-5730-411e-9285-ce4be825cd5c"
        },
        {
            "id": "a67fdb59-c12d-4987-bcf9-93a378a7d93d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "1d5b9b6e-5730-411e-9285-ce4be825cd5c"
        },
        {
            "id": "e56b6267-4c9b-42bc-9fb4-67b51da6a733",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "1d5b9b6e-5730-411e-9285-ce4be825cd5c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "6ee4a807-06a4-4bb1-befb-2bcb9093598a",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}