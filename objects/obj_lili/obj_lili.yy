{
    "id": "0b2312d0-7172-4de2-93c4-0752d17ac786",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_lili",
    "eventList": [
        {
            "id": "e7d00b2c-896c-407c-b338-88ff28133d69",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "0b2312d0-7172-4de2-93c4-0752d17ac786"
        },
        {
            "id": "875c0cb7-ff38-413c-8cd5-e62fbf1976d8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "0b2312d0-7172-4de2-93c4-0752d17ac786"
        }
    ],
    "maskSpriteId": "a727deeb-e8bf-489a-b226-350119fdebc0",
    "overriddenProperties": null,
    "parentObjectId": "152d457d-c505-4423-8dc8-16bf25862791",
    "persistent": false,
    "physicsAngularDamping": 0,
    "physicsDensity": 0.1,
    "physicsFriction": 0,
    "physicsGroup": 0,
    "physicsKinematic": true,
    "physicsLinearDamping": 0,
    "physicsObject": false,
    "physicsRestitution": 0,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "350c7b50-c6a0-41e1-8db2-769c1701da9e",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "442e5eef-7fcd-4044-b96e-f964b877598f",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 16,
            "y": 0
        },
        {
            "id": "55ef47c5-d315-4ba0-809a-0b20b6c2945e",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 16,
            "y": 16
        },
        {
            "id": "26e53b49-05d3-442c-9e41-7c8cd7a5985f",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 16
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e7838b94-f9d2-4cdf-8451-5b12179fab60",
    "visible": true
}