{
    "id": "d3d394b3-89be-4c68-9de2-9856f09bb16d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_ice_skates",
    "eventList": [
        {
            "id": "d2e97bcf-c2af-4f43-8fe4-04cad57c3db5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d3d394b3-89be-4c68-9de2-9856f09bb16d"
        },
        {
            "id": "e2fb8ce3-f06c-498c-a140-2348028b569f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "d3d394b3-89be-4c68-9de2-9856f09bb16d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "270cd060-0fd8-4193-b841-7ed2c3f6a85d",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        {
            "id": "95caae83-1987-4a82-83e3-513a4a831f0a",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "574b70a3-62fa-4285-93da-13319a3c4146",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 16,
            "y": 16
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "68200385-a467-48bc-aff5-8775f11e723b",
    "visible": true
}