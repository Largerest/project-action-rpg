if spr[cs] != noone{
    var c = c_white;
    draw_sprite_ext(spr_person_shadow,0,x,y,1,1,0,c,0.5);
    draw_sprite_ext(spr[cs],pos,x,y-z,xscale*size,yscale*size,rot,c,1);
}

draw_set_font(fnt_pixel_2);
draw_set_center();
var c = c_white;
draw_text_colour(x,y-16-z,string_hash_to_newline(name),c,c,c,c,name_alpha);
draw_reset_alignment();
draw_reset_font();

