{
    "id": "6ee4a807-06a4-4bb1-befb-2bcb9093598a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_obj",
    "eventList": [
        {
            "id": "5cf35560-5214-4059-94a8-607053d76547",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6ee4a807-06a4-4bb1-befb-2bcb9093598a"
        },
        {
            "id": "6908d725-6707-4259-84b7-8f5db6fee58e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "6ee4a807-06a4-4bb1-befb-2bcb9093598a"
        },
        {
            "id": "a969aca0-1ef1-4a34-99ca-65ae8c763e85",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "6ee4a807-06a4-4bb1-befb-2bcb9093598a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        {
            "id": "203b8c74-c921-465c-888b-95839e123d01",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "8502b5e8-9a2e-4e9c-b697-7fed54a93146",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 8,
            "y": 8
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}