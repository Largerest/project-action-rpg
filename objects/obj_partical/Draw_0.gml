alarm_manage();
if owner != noone{
    x = owner.x;
    y = owner.y;
    depth = owner.depth-1;
}
//active = sin(step/30);

if active{
    if !burst{
        if !alarm_active(0){
            i = max(2.5,irandom(ep_max));
            ep[i,A] = irandom(ep_life);
            ep[i,X] = x+ep_bpx+irandom_range(-ep_prh,ep_prh);
            ep[i,Y] = y+ep_bpy+irandom_range(-ep_prv,ep_prv);
            ep[i,S] = random_range(ep_mns,ep_mxs);
            ep[i,D] = irandom_range(ep_mnd,ep_mxd);
            ep[i,Si] = random_range(ep_mnsi,ep_mxsi);
            ep[i,R] = irandom_range(ep_mnr,ep_mxr);
            a[0] = ep_uds;
        }
        i = loop(i,0,ep_max);
    }else{
        alarm[0] = 30;
        active = 0;
    }
}

for (var e = 0;e<=ep_max;e++){
    ep[e,A] = apr(ep[e,A],0,0.1);
    
    var c = c_white;
    ep[e,X] += lengthdir_x(ep[e,S],ep[e,D]);
    ep[e,Y] += lengthdir_y(ep[e,S],ep[e,D]);
    if -ep[e,R]
    draw_sprite_ext(ep_spr,0,ep[e,X],ep[e,Y],ep[e,Si],ep[e,Si],ep[e,D],c,ceil(ep[e,A]));
    else
    draw_sprite_ext(ep_spr,0,ep[e,X],ep[e,Y],ep[e,Si],ep[e,Si],ep[e,R],c,ep[e,A]/ep_life);
    
    draw_set_halign(fa_right);
    draw_set_font(fnt_pixel_2);
    draw_text_colour(x,y,string_hash_to_newline(5),c,c,c,c,1);
    draw_reset_alignment();
    draw_reset_font();
    
}

