owner = noone;
preset = "";
ep_max = 10;
ep_uds = 2;
active = 1;
burst = 0;

ep_spr = ef_dust;
ep_life = 4;
ep_bpx = 0;
ep_bpy = 0;
ep_prh = 2;
ep_prv = 2;
ep_mns = .25;
ep_mxs = .75;
ep_mnd = 45;
ep_mxd = 90+45;
ep_mnsi = 1;
ep_mxsi = 2;
ep_mnr = 0;
ep_mxr = 0;

A = 0;//0-1
X = 1;
Y = 2;
S = 3;
D = 4;
Si = 5;
R = 7;

a[0] = -1;
for (var e = 0;e<=ep_max;e++){
    if active || burst{
        ep[e,A] = 0;
        ep[e,X] = x+ep_bpx+irandom_range(-ep_prh,ep_prh);
        ep[e,Y] = y+ep_bpy+irandom_range(-ep_prv,ep_prv);
        ep[e,S] = random_range(ep_mns,ep_mxs);
        ep[e,D] = irandom_range(ep_mnd,ep_mxd);
        ep[e,Si] = random_range(ep_mnsi,ep_mxsi);
        ep[e,R] = irandom_range(ep_mnr,ep_mxr);
        
        if e = 0{
            ep[e,A] = 0;
            a[0] = ep_uds;
        }
    }else{
        ep[e,A] = 0;
        ep[e,X] = 0;
        ep[e,Y] = 0;
        ep[e,S] = 0;
        ep[e,D] = 0;
        ep[e,Si] = 0;
        ep[e,R] = 0;
    }
}
i = 0;

