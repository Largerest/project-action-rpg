{
    "id": "270cd060-0fd8-4193-b841-7ed2c3f6a85d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_item",
    "eventList": [
        {
            "id": "4305ee41-2bc6-48aa-9c6d-c4c0906258fc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "270cd060-0fd8-4193-b841-7ed2c3f6a85d"
        },
        {
            "id": "d5cc668a-7e69-4511-9b72-9b510ce0097a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "270cd060-0fd8-4193-b841-7ed2c3f6a85d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "6ee4a807-06a4-4bb1-befb-2bcb9093598a",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        {
            "id": "cc4d6f0e-fc09-4652-b1e9-0121dd972f6a",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "92ef863b-b3d2-40af-b59f-d1fc86a7f854",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 16,
            "y": 16
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "68200385-a467-48bc-aff5-8775f11e723b",
    "visible": true
}