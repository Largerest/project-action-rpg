z = (sin(step/15)*2)+5;

if instance_place(x,y,obj_dungeon_person) && taken = 0{
    instance_create(x,y,obj_flicker);
    owner = instance_place(x,y,obj_dungeon_person);
    owner.speed_item = id;
    taken = 1;
}

if owner != noone{
    if owner.speed_item != id{
        instance_destroy();
    }
}

