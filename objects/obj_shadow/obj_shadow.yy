{
    "id": "932310c9-0c8d-4cf7-9151-0dfa122e445f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_shadow",
    "eventList": [
        {
            "id": "b2ba9fb1-5f02-436a-8cc6-96654237df26",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "932310c9-0c8d-4cf7-9151-0dfa122e445f"
        },
        {
            "id": "28414d0a-07ba-4337-a33f-0f6459f4d1b9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "932310c9-0c8d-4cf7-9151-0dfa122e445f"
        },
        {
            "id": "391e8746-3a76-49b1-ac7f-5cb2029dbd7d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "932310c9-0c8d-4cf7-9151-0dfa122e445f"
        }
    ],
    "maskSpriteId": "a727deeb-e8bf-489a-b226-350119fdebc0",
    "overriddenProperties": null,
    "parentObjectId": "a1811898-5c11-4dd8-802b-9c39e9a64265",
    "persistent": false,
    "physicsAngularDamping": 0,
    "physicsDensity": 0.1,
    "physicsFriction": 0,
    "physicsGroup": 0,
    "physicsKinematic": true,
    "physicsLinearDamping": 0,
    "physicsObject": false,
    "physicsRestitution": 0,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "a6300673-bc3d-4df7-9023-541f5c8805ea",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "85ffc8f9-ac8a-4c11-8569-7cd89d29494d",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 16,
            "y": 0
        },
        {
            "id": "3b8e0326-12ff-4158-a093-67bf2c3a39de",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 16,
            "y": 16
        },
        {
            "id": "89c7e9a1-77e2-4863-9395-b68709b13a7b",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 16
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "6418ffe6-3055-4ecc-83b3-1b39c5230237",
    "visible": true
}