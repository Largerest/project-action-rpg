action_inherited();
//movement properties
name = "Shadow";

sms = 1.5;
acc = 0.5;
dcc = 0.5;
tmpacc = dcc;
z = 0;

//controler properties
controller = noone;

target = instance_nearest(x,y,obj_dungeon_person);

spr[0] = spr_shadow_monster;
is = 0.1;
istop = 6;
ls[0,0] = 0;
ls[0,1] = 2;
ls[1,0] = 3;
ls[2,0] = 4;
ls[2,1] = 5.75;
awake = 0;

scr_partical_create(0,"Shadow Aura");

