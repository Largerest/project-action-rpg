{
    "id": "487f74b5-a9a3-4203-9d69-16abef718aa6",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_solid",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "6ee4a807-06a4-4bb1-befb-2bcb9093598a",
    "persistent": false,
    "physicsAngularDamping": 0,
    "physicsDensity": 0,
    "physicsFriction": 0,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0,
    "physicsObject": false,
    "physicsRestitution": 0,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "819b1d38-ef22-4913-8804-82b4598ec8f1",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": -8,
            "y": -8
        },
        {
            "id": "236fa4bc-e3d4-484e-a10e-094c9e477c09",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 8,
            "y": -8
        },
        {
            "id": "00322a27-e5b9-43c2-8061-f08d8a79b0fe",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 8,
            "y": 8
        },
        {
            "id": "267b0a64-c7d7-4f7e-b72c-f0cde63047f5",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": -8,
            "y": 8
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "1201a107-eaf7-444f-81db-db2d584827c5",
    "visible": false
}