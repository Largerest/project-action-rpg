{
    "id": "ffec246b-f4b9-4ce3-87ab-4791ff9bdf6e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "sys_dj",
    "eventList": [
        {
            "id": "2d738a4d-eced-4968-98ba-e0c3e9f2ade5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ffec246b-f4b9-4ce3-87ab-4791ff9bdf6e"
        },
        {
            "id": "17dba9df-2e0b-4211-91ad-d45f1ccd49b7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "ffec246b-f4b9-4ce3-87ab-4791ff9bdf6e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        {
            "id": "34f4eb26-75b7-423c-9a96-48217b346d6a",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "b5117e92-1206-4d30-8477-9d137deafa69",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 8,
            "y": 8
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "0404758d-a21f-416c-9800-2c3c7718040c",
    "visible": false
}