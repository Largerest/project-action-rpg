{
    "id": "ce7e3600-17d3-4404-9440-bcf6e41203c9",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_ferrosia",
    "eventList": [
        {
            "id": "0b84c23d-d07c-4b25-a7a5-86a8d4546e71",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ce7e3600-17d3-4404-9440-bcf6e41203c9"
        },
        {
            "id": "da39f286-40b6-43ec-9b25-091c80c1e8b0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "ce7e3600-17d3-4404-9440-bcf6e41203c9"
        }
    ],
    "maskSpriteId": "a727deeb-e8bf-489a-b226-350119fdebc0",
    "overriddenProperties": null,
    "parentObjectId": "152d457d-c505-4423-8dc8-16bf25862791",
    "persistent": false,
    "physicsAngularDamping": 0,
    "physicsDensity": 0.1,
    "physicsFriction": 0,
    "physicsGroup": 0,
    "physicsKinematic": true,
    "physicsLinearDamping": 0,
    "physicsObject": false,
    "physicsRestitution": 0,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "34b52256-4072-4d1c-a644-9969f19dc9d0",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "8999ec8c-d222-429f-aeaf-88ff3e681a38",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 16,
            "y": 0
        },
        {
            "id": "f27808bd-7a68-48a9-8aee-3d6d890082b8",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 16,
            "y": 16
        },
        {
            "id": "81fec196-c14c-4660-a225-ab791ac0d136",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 16
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d4c6ef0b-557c-442f-88be-59e0ff33a52f",
    "visible": true
}