{
    "id": "352fd9bc-4b6f-43bc-a17d-a77293ad46f5",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_shadow_puddle",
    "eventList": [
        {
            "id": "9f95f4a3-434b-4ef5-a68c-fe1abcff847e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "352fd9bc-4b6f-43bc-a17d-a77293ad46f5"
        },
        {
            "id": "ee7fca8e-fdf3-4f3c-8505-368f6683b02e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "352fd9bc-4b6f-43bc-a17d-a77293ad46f5"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "6ee4a807-06a4-4bb1-befb-2bcb9093598a",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        {
            "id": "afc203ab-6a3c-4d45-9d99-edcfcdf7ecf6",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 19,
            "y": 6
        },
        {
            "id": "ba142472-b7bb-431f-a9cd-13f762a97cdd",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 20,
            "y": 20
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "285155b0-6f9e-4dd5-8326-cbd6a1bd73de",
    "visible": true
}