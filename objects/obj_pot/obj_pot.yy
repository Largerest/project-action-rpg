{
    "id": "d3d86e91-7a4f-4852-9692-45a166c1a7a1",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_pot",
    "eventList": [
        {
            "id": "1203732b-c9e3-4816-99a3-44a0f0635237",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d3d86e91-7a4f-4852-9692-45a166c1a7a1"
        },
        {
            "id": "1720ca1c-0a2e-4f4d-a0ff-408ae03b29e4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "d3d86e91-7a4f-4852-9692-45a166c1a7a1"
        },
        {
            "id": "f36c72b3-06ac-4127-92b7-2b5226602ead",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "d3d86e91-7a4f-4852-9692-45a166c1a7a1"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "487f74b5-a9a3-4203-9d69-16abef718aa6",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        {
            "id": "e9dad65a-e7a2-46dc-b1e3-248782eaa29f",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 8,
            "y": 16
        },
        {
            "id": "1b061382-3986-400c-b302-56b9f8bf180e",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 8,
            "y": 8
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a86ff19d-0441-4904-a188-d0cc99a2ea04",
    "visible": true
}