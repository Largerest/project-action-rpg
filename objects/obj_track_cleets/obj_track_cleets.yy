{
    "id": "9800f549-077f-474b-bb05-702608562269",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_track_cleets",
    "eventList": [
        {
            "id": "a9c6bf3f-a595-4156-8859-c3aad3d6a772",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "9800f549-077f-474b-bb05-702608562269"
        },
        {
            "id": "89adb464-8809-465b-b2d7-cf160366dd83",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "9800f549-077f-474b-bb05-702608562269"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "270cd060-0fd8-4193-b841-7ed2c3f6a85d",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        {
            "id": "dc74cdea-456b-4fbc-8f0d-d8b1e139f054",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "ae8ff188-b378-49a0-8759-3c8c9be65e0a",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 16,
            "y": 16
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "68200385-a467-48bc-aff5-8775f11e723b",
    "visible": true
}