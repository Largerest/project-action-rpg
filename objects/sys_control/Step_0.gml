surf_update("gui",game_width,game_height);

debug(ord("R"),vk_escape);

//Game Resolution
if window_get_width() != game_width*zoom && window_get_height() != game_height*zoom{
    display_set_gui_size(__view_get( e__VW.WView, 0 ),__view_get( e__VW.HView, 0 ));
    window_set_size(game_width*zoom, game_height*zoom);
    surface_resize(application_surface,game_width,game_height);
    display_reset(0,false);
}

view_scale = clamp(view_scale,0.1,3);
view_scale += (-kbc(vk_numpad3,1)+kbc(vk_numpad4,1))/10;

zoom = clamp(zoom,1,3);
zoom += (-kbc(vk_subtract,1)+kbc(vk_add,1));

__view_set( e__VW.WView, 0, game_width*view_scale );
__view_set( e__VW.HView, 0, game_height*view_scale );




