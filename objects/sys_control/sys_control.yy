{
    "id": "21a79df5-4040-4aa4-b761-a88ed0096a10",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "sys_control",
    "eventList": [
        {
            "id": "e77a2ee2-5956-40f9-af5d-d5db4c6de3a9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "21a79df5-4040-4aa4-b761-a88ed0096a10"
        },
        {
            "id": "6b983719-9de7-4e4f-aaff-0419c112e5c8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "21a79df5-4040-4aa4-b761-a88ed0096a10"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        {
            "id": "388fe143-520f-4150-95fe-d2749455599d",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "71351755-833b-4221-b97b-d90e888c5a6a",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 16,
            "y": 16
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "babf9ef0-6d96-4010-88ce-1ba0aea9a5d6",
    "visible": false
}