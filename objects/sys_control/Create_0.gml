/// @description Game Setup
instance_create(0,0,sys_layers);
instance_create(0,0,sys_controls);
instance_create(0,0,sys_menu);
instance_create(0,0,sys_time);
instance_create(0,0,sys_dj);

//Game Resolution
globalvar game_width,game_height,zoom,view_scale;
game_width = 320;
game_height = 240;
zoom = 2;
view_scale = 1;
surf_add("gui",game_width,game_height);
display_set_gui_size(__view_get( e__VW.WView, 0 ),__view_get( e__VW.HView, 0 ));
window_set_size(game_width*zoom, game_height*zoom);
surface_resize(application_surface,game_width,game_height);
display_reset(0,false);

__view_set( e__VW.WPort, 0, game_width*view_scale );
__view_set( e__VW.HView, 0, game_height*view_scale );
__view_set( e__VW.WView, 0, game_width*view_scale );
__view_set( e__VW.HView, 0, game_height*view_scale );
room_speed = 60;

