{
    "id": "a98508af-e2ef-4d47-b1ff-cecf6a165aaf",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "track",
    "eventList": [
        {
            "id": "aeb37dd8-37ff-49f3-a0d5-6e36e9c24538",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a98508af-e2ef-4d47-b1ff-cecf6a165aaf"
        },
        {
            "id": "8f5314c0-9829-467a-a9bd-f243d43629bc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "a98508af-e2ef-4d47-b1ff-cecf6a165aaf"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        {
            "id": "96146d96-867f-4f5f-9e8a-79b14fdbf77e",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 3,
            "y": 3
        },
        {
            "id": "39a080ff-9adb-4e63-80be-838062d29a06",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 3,
            "y": 3
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "6c29ab89-16df-435b-afd8-58aa9fb341ca",
    "visible": true
}