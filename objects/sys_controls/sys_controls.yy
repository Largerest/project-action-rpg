{
    "id": "a3ab6451-a3c1-4b4b-97ba-b596ad7756df",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "sys_controls",
    "eventList": [
        {
            "id": "114ad047-b178-416c-9566-ba053a92c4a0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a3ab6451-a3c1-4b4b-97ba-b596ad7756df"
        },
        {
            "id": "1ae6f657-5f10-4048-81fd-bb5d9c2823f8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "a3ab6451-a3c1-4b4b-97ba-b596ad7756df"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        {
            "id": "e6bff146-066b-4931-bca7-00be053037da",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "51d582f5-7458-4c85-9c8c-de52bd664d95",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 16,
            "y": 16
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "bbee54ef-f7b8-4fb4-bf00-9f93fce38262",
    "visible": false
}