oleft   = ord("A");
oright  = ord("D");
oup     = ord("W");
odown   = ord("S");
oact1   = ord("J");
oact2   = ord("K");
oact3   = ord("L");
omenu   = vk_enter;

controlling = noone;

globalvar bmenu;
bleft	= kbc(oleft);
bright	= kbc(oright);
bup		= kbc(oup);
bdown	= kbc(odown);
bact1	= kbc(oact1);
bact2	= kbc(oact2);
bact3	= kbc(oact3);
bmenu	= kbc(omenu);

bah = -bleft[0]+bright[0];
bav = -bup[0]+bdown[0];

bdir = point_direction(0,0,bah,bav);
