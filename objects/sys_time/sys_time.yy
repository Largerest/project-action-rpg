{
    "id": "b76f1501-bee4-4872-bf20-5d7ef11bea5a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "sys_time",
    "eventList": [
        {
            "id": "391ed18c-2fe0-461e-bb19-c47e828a1f42",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b76f1501-bee4-4872-bf20-5d7ef11bea5a"
        },
        {
            "id": "18d8910f-a46c-4983-9ed6-317aaa016231",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "b76f1501-bee4-4872-bf20-5d7ef11bea5a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        {
            "id": "ed91aaee-e576-44ad-8d3d-86219afd44a2",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "800c2666-c264-49c2-85a5-a469b730f97c",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 16,
            "y": 16
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "047749e8-b121-4b95-a221-8f499435be25",
    "visible": false
}