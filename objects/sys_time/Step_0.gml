rewind = kbc(vk_left,0);
if rewind{
    step_pause = 1;
}
if kbc(vk_up,1) && !step_pause{
    step_pause = 1;
}else if kbc(vk_up,1) && step_pause{
    step_pause = 0;
}
rec = !rewind;
step += !(pause||step_pause);
step -= (pause||step_pause)&&rewind;
step = max(0,step);

sys_step++;
sys_step = loop(sys_step,0,max_steps);
step = loop(step,0,max_steps);

if pause||step_pause{
    audio_pause_all();
}else{
    audio_resume_all();
}

