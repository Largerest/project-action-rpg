if alarm_sound(0) instance_destroy();
with instance_place(x+h,y+v,obj_pot){
    instance_destroy();
}

if instance_place(x+h,y+v,obj_enemy){
    instance_destroy();
}

var tempvar_col = collision_circle(x,y,32,obj_enemy,0,1);
if tempvar_col{
    var tempvar_dir = point_direction(x,y,tempvar_col.x,tempvar_col.y);
    h = lengthdir_x(firespd,tempvar_dir);
    v = lengthdir_y(firespd,tempvar_dir);
}

