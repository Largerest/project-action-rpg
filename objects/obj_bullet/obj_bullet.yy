{
    "id": "dacdb9a8-955b-406d-a233-8396152ca3bb",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_bullet",
    "eventList": [
        {
            "id": "059c4b10-1d36-45b0-8d70-5f1af3931ab7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "dacdb9a8-955b-406d-a233-8396152ca3bb"
        },
        {
            "id": "1f72ed23-16ef-4716-8049-147f172a2715",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "dacdb9a8-955b-406d-a233-8396152ca3bb"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "6ee4a807-06a4-4bb1-befb-2bcb9093598a",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        {
            "id": "e3e09a53-c091-4659-a8f7-894db6e32f67",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "bd6e5a25-095b-45a0-9d21-a5aae542b21e",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 16,
            "y": 16
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "6c29ab89-16df-435b-afd8-58aa9fb341ca",
    "visible": true
}