{
    "id": "1c2c09b9-3d33-48eb-a7ab-d9d2fad7bc47",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_house",
    "eventList": [
        {
            "id": "c66484b6-c248-4a97-9688-feb672399796",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "1c2c09b9-3d33-48eb-a7ab-d9d2fad7bc47"
        }
    ],
    "maskSpriteId": "9c1d481d-0760-4e9f-a005-af3c31815993",
    "overriddenProperties": null,
    "parentObjectId": "487f74b5-a9a3-4203-9d69-16abef718aa6",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        {
            "id": "ae221360-0f5f-4add-a410-c298822fa2ae",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 40,
            "y": 80
        },
        {
            "id": "0c14d4dc-dbcf-41e3-977f-72a1534958df",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 48,
            "y": 48
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "2d933de5-25bf-40c2-993a-17f777a6a2b6",
    "visible": true
}