{
    "id": "5db91ffd-132d-45ef-af65-f07a822181b2",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "sys_layers",
    "eventList": [
        {
            "id": "6cf5b297-44ac-44aa-a246-fbaa95572809",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "5db91ffd-132d-45ef-af65-f07a822181b2"
        },
        {
            "id": "b7aab504-ca87-4a83-93ac-37304f7d0eb7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "5db91ffd-132d-45ef-af65-f07a822181b2"
        },
        {
            "id": "e3ccf04c-07a5-434e-967b-d41c114f3d53",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 3,
            "eventtype": 7,
            "m_owner": "5db91ffd-132d-45ef-af65-f07a822181b2"
        },
        {
            "id": "705a0703-3c5f-4710-a493-74be30506c0a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "5db91ffd-132d-45ef-af65-f07a822181b2"
        },
        {
            "id": "0a97c298-aa84-440b-bf24-042860584678",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "5db91ffd-132d-45ef-af65-f07a822181b2"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}