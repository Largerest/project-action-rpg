surf_target("gui");
    with (sys_control){
        draw_clear_alpha(c_black,0);
        
        draw_sprite(gui_system,0,16+(16*0),16);
        draw_sprite(gui_time,0,16+(16*1),16);
        draw_sprite(gui_controls,0,16+(16*2),16);
        draw_sprite(gui_menu,0,16+(16*3),16);
        draw_sprite(gui_dj,0,16+(16*4),16);
        draw_reset_alignment();
        draw_set_font(fnt_pixel_2);
        draw_text(16,32,string_hash_to_newline(string(step)+" "+string(fps)));
        draw_reset_font();
    }
    with (sys_menu){
        var c = c_black;
        alpha = apr(alpha,pause*0.75,dif(alpha,pause*0.75,0.20));
        draw_set_alpha(alpha);
        draw_rectangle_colour(0,0,game_width,game_height,c,c,c,c,0);
        draw_set_alpha(1);
        draw_menu();
    }
surface_reset_target();


