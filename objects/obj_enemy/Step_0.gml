action_inherited();
///controlls
target = instance_nearest(x,y,obj_dungeon_person);
my_bah = lengthdir_x(awake,point_direction(x,y,target.x,target.y));
my_bav = lengthdir_y(awake,point_direction(x,y,target.x,target.y));
my_bdir = point_direction(0,0,my_bah,my_bav);

if !(pause||step_pause){
    if abs(my_bah) || abs(my_bav){
        spd = sms;
        tempacc = acc;
    }else{
        spd = 0;
        tempacc = dcc;
    }
    
    h = apr(h,my_bah*spd,tmpacc);
    v = apr(v,my_bav*spd,tmpacc);
    
    
    with obj_bullet{
        var col = instance_place(x+h,y+v,obj_enemy);
        with col{
            var tempvar_dir = point_direction(x,y,other.x,other.y);
            h = lengthdir_x(-0.75,tempvar_dir);
            v = lengthdir_x(-0.75,tempvar_dir);
            if !audio_is_playing(snd_shadow_hit)
            audio_play_sound(snd_shadow_hit,0,0);
            instance_destroy();
        }
    }
}

