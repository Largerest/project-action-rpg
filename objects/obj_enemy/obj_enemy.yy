{
    "id": "a1811898-5c11-4dd8-802b-9c39e9a64265",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_enemy",
    "eventList": [
        {
            "id": "5fc4f1e3-c284-4493-b5c2-82f99d143b9d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a1811898-5c11-4dd8-802b-9c39e9a64265"
        },
        {
            "id": "3240a45e-7ce0-44f3-886b-576c34a5093e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "a1811898-5c11-4dd8-802b-9c39e9a64265"
        }
    ],
    "maskSpriteId": "a727deeb-e8bf-489a-b226-350119fdebc0",
    "overriddenProperties": null,
    "parentObjectId": "6ee4a807-06a4-4bb1-befb-2bcb9093598a",
    "persistent": false,
    "physicsAngularDamping": 0,
    "physicsDensity": 0.1,
    "physicsFriction": 0,
    "physicsGroup": 0,
    "physicsKinematic": true,
    "physicsLinearDamping": 0,
    "physicsObject": false,
    "physicsRestitution": 0,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "39940d43-57bb-47ac-ab89-7b8e5bcbf93d",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "f9146132-367c-4a26-8784-42f5ee0dc1fd",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 16,
            "y": 0
        },
        {
            "id": "374bcf17-8e4f-459f-8ef4-0be7e1b173f2",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 16,
            "y": 16
        },
        {
            "id": "9e9b5911-e849-4552-a775-74f4d8713ab1",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 16
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}