{
    "id": "3f996826-9713-46bf-ac52-b7980b180aeb",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_slitherer",
    "eventList": [
        {
            "id": "5409db1e-a4d4-47f6-84d7-9dca205f1292",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3f996826-9713-46bf-ac52-b7980b180aeb"
        },
        {
            "id": "b134ba8d-29f9-41d4-9f07-7ec57f8d0650",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "3f996826-9713-46bf-ac52-b7980b180aeb"
        }
    ],
    "maskSpriteId": "a727deeb-e8bf-489a-b226-350119fdebc0",
    "overriddenProperties": null,
    "parentObjectId": "a1811898-5c11-4dd8-802b-9c39e9a64265",
    "persistent": false,
    "physicsAngularDamping": 0,
    "physicsDensity": 0.1,
    "physicsFriction": 0,
    "physicsGroup": 0,
    "physicsKinematic": true,
    "physicsLinearDamping": 0,
    "physicsObject": false,
    "physicsRestitution": 0,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "826329f7-9116-4609-8b62-9584f2fdb635",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "1bb5a273-7294-4f64-a682-0a891792a530",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 16,
            "y": 0
        },
        {
            "id": "b90c39be-9979-4be0-ab3a-2433a447393b",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 16,
            "y": 16
        },
        {
            "id": "27ceb501-bcf7-492b-9947-7d0f18956f94",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 16
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "26eeae74-6817-412b-8847-0070d0140722",
    "visible": true
}