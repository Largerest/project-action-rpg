{
    "id": "f6f744c9-e1d6-44de-bbfd-5168945f7dee",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "sys_menu",
    "eventList": [
        {
            "id": "da6cec9c-30d2-4206-a59c-afacefcc454c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f6f744c9-e1d6-44de-bbfd-5168945f7dee"
        },
        {
            "id": "aebf20c2-6b6f-444e-8c19-27ad036df4f6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "f6f744c9-e1d6-44de-bbfd-5168945f7dee"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        {
            "id": "83d85035-ca5e-4624-ab7b-9e5bb55ee23a",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 24,
            "y": 8
        },
        {
            "id": "d7aff29a-5aee-44c3-b0fb-f04abf88fbfd",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 24,
            "y": 24
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "70710539-6765-485d-a114-31f61330a377",
    "visible": false
}