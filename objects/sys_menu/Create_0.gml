globalvar pause;
pause = 0;
alpha = 0;

width = 2;
height = 0;

for (var i = 0;i<=width;i++){
    for (var e = 0;e<=height;e++){
        menu_func[i,e] = btn_func_do_nothing;
        menu_spr_1[i,e] = spr_button_us;
        menu_spr_2[i,e] = spr_button_se;
        menu_txt[i,e] = "blank";
    }
}

menu_func[0,0] = btn_func_resume_game;
menu_func[1,0] = btn_func_restart_game;
menu_func[2,0] = btn_func_end_game;

menu_txt[0,0] = "Resume";
menu_txt[1,0] = "Restart";
menu_txt[2,0] = "Exit";

_x = 0;
_y = 1;
cursor[_x] = 0;
cursor[_y] = 0;

