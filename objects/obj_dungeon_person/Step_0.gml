/// @description controlls

///Main thang
if !(pause||step_pause){
    if speed_item = noone{
        sms = bsms;
        acc = bacc;
        dcc = bdcc;
    }

    if abs(my_bah){
        h = apr(h,lengthdir_x(sms*size,my_bdir),acc);
    }else{
        h = apr(h,0,dcc);
    }
    if abs(my_bav){
        v = apr(v,lengthdir_y(sms*size,my_bdir),acc);
    }else{
        v = apr(v,0,dcc);
    }
    
    if kbc("J",1) {
        audio_play_sound(snd_gun,0,0);
        with instance_create(x,y,obj_bullet){
            h = lengthdir_x(firespd,other.my_bdir);
            v = lengthdir_y(firespd,other.my_bdir);
        }
    }
}

///Sprites
if !(pause||step_pause){
    if abs(h)>acc||abs(v)>acc{
        state = run;
        if abs(sign(my_bah)) xscale = sign(my_bah);
        part[0].active = 1;
    }else{
        state = idle;
        part[0].active = 0;
    }
    cs = state;
    is = 0.2/size;
    if cs = run && (round(pos) = 1 || round(pos) = 4){
        if !alarm_active(0){
            audio_play_sound(snd_footstep,0,0);
            alarm_set_time(0,(room_speed*is)*size);
        }
    }else{
        alarm_set_time(0,0);
    }
}

