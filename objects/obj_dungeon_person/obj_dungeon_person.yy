{
    "id": "152d457d-c505-4423-8dc8-16bf25862791",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_dungeon_person",
    "eventList": [
        {
            "id": "da3e7b3e-b25a-4952-8e34-ea8b83a15af6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "152d457d-c505-4423-8dc8-16bf25862791"
        },
        {
            "id": "46661a68-122c-4a5d-b7d5-c4e65b4b7abd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "152d457d-c505-4423-8dc8-16bf25862791"
        },
        {
            "id": "0af73d2f-0a6e-4d6f-83ed-7d3b1ed51f55",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "152d457d-c505-4423-8dc8-16bf25862791"
        }
    ],
    "maskSpriteId": "a727deeb-e8bf-489a-b226-350119fdebc0",
    "overriddenProperties": null,
    "parentObjectId": "6ee4a807-06a4-4bb1-befb-2bcb9093598a",
    "persistent": false,
    "physicsAngularDamping": 0,
    "physicsDensity": 0.1,
    "physicsFriction": 0,
    "physicsGroup": 0,
    "physicsKinematic": true,
    "physicsLinearDamping": 0,
    "physicsObject": false,
    "physicsRestitution": 0,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "b0bbcd63-6d73-4a3c-b623-795996df68aa",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "738adf0a-dcfe-4463-abef-f2ca135e7683",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 16,
            "y": 0
        },
        {
            "id": "69db34e3-597b-4415-89ec-267d7dea3a55",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 16,
            "y": 16
        },
        {
            "id": "47591210-263f-489d-acf5-7f4557adb90b",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 16
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e7838b94-f9d2-4cdf-8451-5b12179fab60",
    "visible": true
}