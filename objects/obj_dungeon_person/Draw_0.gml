surf_target("gui");
var c = c_white;
draw_text_colour(game_width-16,16,string_hash_to_newline(money),c,c,c,c,1);
surface_reset_target();

if state = idle{
    name_alpha = apr(name_alpha,point_in_rectangle(x,y,__view_get( e__VW.XView, 0 )+32,__view_get( e__VW.YView, 0 )+32,__view_get( e__VW.XView, 0 )+__view_get( e__VW.WView, 0 )-32,__view_get( e__VW.YView, 0 )+__view_get( e__VW.HView, 0 )-32),0.1);
}else if state = run{
    name_alpha = apr(name_alpha,0,0.1);
}

draw_sprite_ext(ui_pointer,0,x,y,1,1,my_bdir,c_white,(abs(my_bah)||abs(my_bav))*0.5);

action_inherited();
