{
    "id": "d6e569cb-8c51-4a51-92ce-55b03815d045",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_super_mushroom",
    "eventList": [
        {
            "id": "4b2c9e08-a307-47e0-a9b1-07eb6a9fff65",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d6e569cb-8c51-4a51-92ce-55b03815d045"
        },
        {
            "id": "d95acfa3-ecd4-474f-a219-a5c2e5702046",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "d6e569cb-8c51-4a51-92ce-55b03815d045"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "6ee4a807-06a4-4bb1-befb-2bcb9093598a",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        {
            "id": "b9725cd0-1142-47c3-a73a-e743889476c6",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 8,
            "y": 16
        },
        {
            "id": "953a234b-ddd6-4352-9774-236eba77be23",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 8,
            "y": 8
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "827d0105-fdb4-4e6e-aabc-37d013293dca",
    "visible": true
}