{
    "id": "fa32af38-2745-4bb7-ad46-f0286d754445",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_coin",
    "eventList": [
        {
            "id": "2ace754e-ec14-4a47-849c-2f625f460d75",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "fa32af38-2745-4bb7-ad46-f0286d754445"
        },
        {
            "id": "d120c91c-a9c4-4061-8217-c6006825b221",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "fa32af38-2745-4bb7-ad46-f0286d754445"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "6ee4a807-06a4-4bb1-befb-2bcb9093598a",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        {
            "id": "90aea44c-4514-4c51-a51b-b495ace7ffdb",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 3,
            "y": 6
        },
        {
            "id": "866a0649-d560-40e2-8d1b-2a6b566e056a",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 3,
            "y": 3
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "bfd49cea-e79c-4bd1-9d1e-2b3488ced258",
    "visible": true
}