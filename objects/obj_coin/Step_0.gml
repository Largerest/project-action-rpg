if !(pause||step_pause){
    zs = apr(zs,-10,0.25);
    z += zs;
    
    z = max(0,z);
    if z <= 0 zs*=-0.75;
    
    var col = collision_circle(x,y,32,obj_dungeon_person,0,1);
    if col != noone{
        dir = point_direction(x,y,col.x,col.y);
        spd = col.sms;
        h = apr(h,lengthdir_x(spd,dir),1);
        v = apr(v,lengthdir_y(spd,dir),1);
    }else{
        h = apr(h,0,dcc);
        v = apr(v,0,dcc);
    }
    
    if place_meeting(x,y,col){
        with instance_place(x,y,col){
            money += 5;
        }
        audio_play_sound(snd_coin_collect,0,0);
        instance_destroy();
    }
}

