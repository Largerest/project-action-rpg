{
    "id": "2baa7b29-b3b9-43bc-bbd8-3b234c53fbd7",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_ai",
    "eventList": [
        {
            "id": "07becadc-2f34-4fdf-8da8-c74ee40803c7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2baa7b29-b3b9-43bc-bbd8-3b234c53fbd7"
        },
        {
            "id": "1a9441be-8711-476d-9f15-f4b28513a1b9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "2baa7b29-b3b9-43bc-bbd8-3b234c53fbd7"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "a3ab6451-a3c1-4b4b-97ba-b596ad7756df",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        {
            "id": "e660473b-e8ee-496c-a222-0a0ea14f3e12",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "bbe76f35-4db3-482c-8888-1c365efb36c9",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 8,
            "y": 8
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "bbee54ef-f7b8-4fb4-bf00-9f93fce38262",
    "visible": false
}