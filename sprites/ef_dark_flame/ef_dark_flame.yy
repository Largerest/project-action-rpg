{
    "id": "6c5623b2-ce40-4acc-aa7f-73612ee460aa",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "ef_dark_flame",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 3,
    "bbox_left": 0,
    "bbox_right": 3,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8bb62197-e31d-4a08-8732-be49de7befd4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6c5623b2-ce40-4acc-aa7f-73612ee460aa",
            "compositeImage": {
                "id": "9a8d88b9-cf4a-4558-9e77-04928f05e30c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8bb62197-e31d-4a08-8732-be49de7befd4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3ac7783b-29d7-4113-bc74-41dfeba8347e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8bb62197-e31d-4a08-8732-be49de7befd4",
                    "LayerId": "4125bb36-ae25-4023-a504-70a2056fb1b8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 4,
    "layers": [
        {
            "id": "4125bb36-ae25-4023-a504-70a2056fb1b8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6c5623b2-ce40-4acc-aa7f-73612ee460aa",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 4,
    "xorig": 2,
    "yorig": 2
}