{
    "id": "28cc3005-37cf-49cc-9fb8-bc2ed4b61224",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "tls_dungeon",
    "For3D": false,
    "HTile": true,
    "VTile": true,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 143,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c9266348-bca1-4861-b7fe-860374c7e18c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28cc3005-37cf-49cc-9fb8-bc2ed4b61224",
            "compositeImage": {
                "id": "e2742773-f8a2-459f-be8d-d553711d9088",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c9266348-bca1-4861-b7fe-860374c7e18c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "69ea3efc-2f63-4767-a324-03d43476cb8e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c9266348-bca1-4861-b7fe-860374c7e18c",
                    "LayerId": "f8153c27-c68b-4d72-8d5b-247f5de2dc38"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "f8153c27-c68b-4d72-8d5b-247f5de2dc38",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "28cc3005-37cf-49cc-9fb8-bc2ed4b61224",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 160,
    "xorig": 0,
    "yorig": 0
}