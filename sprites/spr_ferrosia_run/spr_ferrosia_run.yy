{
    "id": "89e613b2-1b16-48d8-83be-de2e83b323b3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ferrosia_run",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 20,
    "bbox_left": 0,
    "bbox_right": 12,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "92a0fd6b-d3f6-44df-916f-e850f6214b00",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "89e613b2-1b16-48d8-83be-de2e83b323b3",
            "compositeImage": {
                "id": "04ed2d22-d1b9-4d01-8572-64607ae841ee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "92a0fd6b-d3f6-44df-916f-e850f6214b00",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "636a88d4-786d-4e35-b7d4-4480dc644e98",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "92a0fd6b-d3f6-44df-916f-e850f6214b00",
                    "LayerId": "3b149834-1cd7-459b-acc6-716e28e04fd3"
                }
            ]
        },
        {
            "id": "122654e8-ff87-4e0d-8ab7-941d012297ab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "89e613b2-1b16-48d8-83be-de2e83b323b3",
            "compositeImage": {
                "id": "12ee28ad-0442-4fff-b687-d0e109950dc3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "122654e8-ff87-4e0d-8ab7-941d012297ab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "245500e3-ddf6-4831-bda3-08352f1994d5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "122654e8-ff87-4e0d-8ab7-941d012297ab",
                    "LayerId": "3b149834-1cd7-459b-acc6-716e28e04fd3"
                }
            ]
        },
        {
            "id": "d6c9414a-4deb-451f-83ae-9a4d1d8ebc14",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "89e613b2-1b16-48d8-83be-de2e83b323b3",
            "compositeImage": {
                "id": "8b4357f2-d73c-462b-add7-805435bc22e4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d6c9414a-4deb-451f-83ae-9a4d1d8ebc14",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fcfeb149-009c-4e03-b539-a4999af6e204",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d6c9414a-4deb-451f-83ae-9a4d1d8ebc14",
                    "LayerId": "3b149834-1cd7-459b-acc6-716e28e04fd3"
                }
            ]
        },
        {
            "id": "bc63416f-5e62-4431-aa69-0af457f66cc8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "89e613b2-1b16-48d8-83be-de2e83b323b3",
            "compositeImage": {
                "id": "2818a6ae-651a-4fd8-ba17-83924d6ccf82",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bc63416f-5e62-4431-aa69-0af457f66cc8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "015bb11c-4c28-42a3-8024-c334e7db85b4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bc63416f-5e62-4431-aa69-0af457f66cc8",
                    "LayerId": "3b149834-1cd7-459b-acc6-716e28e04fd3"
                }
            ]
        },
        {
            "id": "59f61e49-331e-4763-ade6-878013fcbded",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "89e613b2-1b16-48d8-83be-de2e83b323b3",
            "compositeImage": {
                "id": "7458dc31-e50e-4704-a352-847a868f4418",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "59f61e49-331e-4763-ade6-878013fcbded",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ecbe80f5-bdc8-4909-9cc1-c663f7b0d98b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "59f61e49-331e-4763-ade6-878013fcbded",
                    "LayerId": "3b149834-1cd7-459b-acc6-716e28e04fd3"
                }
            ]
        },
        {
            "id": "75e64c07-951f-4ced-8b00-b7a50a8d9bcf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "89e613b2-1b16-48d8-83be-de2e83b323b3",
            "compositeImage": {
                "id": "2b71e9df-700e-45a3-9fb2-0a686f1caadc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "75e64c07-951f-4ced-8b00-b7a50a8d9bcf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "937f342a-a95e-402e-a1d0-469a3e9afe4d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "75e64c07-951f-4ced-8b00-b7a50a8d9bcf",
                    "LayerId": "3b149834-1cd7-459b-acc6-716e28e04fd3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 21,
    "layers": [
        {
            "id": "3b149834-1cd7-459b-acc6-716e28e04fd3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "89e613b2-1b16-48d8-83be-de2e83b323b3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 14,
    "xorig": 7,
    "yorig": 21
}