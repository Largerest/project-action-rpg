{
    "id": "a86ff19d-0441-4904-a188-d0cc99a2ea04",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pot",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 8,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1887e828-847d-4f46-8618-42a629ade457",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a86ff19d-0441-4904-a188-d0cc99a2ea04",
            "compositeImage": {
                "id": "f1f0c0ee-5771-44bf-ad9c-4c9d1eae84c7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1887e828-847d-4f46-8618-42a629ade457",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dbab489e-73c4-4cea-9ed4-9a2eaac8a53d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1887e828-847d-4f46-8618-42a629ade457",
                    "LayerId": "778eaf41-e1ea-4fa5-93f9-37201d2f31c9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "778eaf41-e1ea-4fa5-93f9-37201d2f31c9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a86ff19d-0441-4904-a188-d0cc99a2ea04",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}