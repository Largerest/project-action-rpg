{
    "id": "9c1d481d-0760-4e9f-a005-af3c31815993",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "msk_house",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 8,
    "bbox_right": 71,
    "bbox_top": 48,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8b0fc435-647b-4c01-8826-d09401b9adeb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9c1d481d-0760-4e9f-a005-af3c31815993",
            "compositeImage": {
                "id": "96205570-027e-432e-a496-e500636374b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8b0fc435-647b-4c01-8826-d09401b9adeb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e5a6ff45-4947-4ec1-8897-0a9e3ce9d82c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8b0fc435-647b-4c01-8826-d09401b9adeb",
                    "LayerId": "c4af667f-8d65-4dc5-bc51-573af3611122"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "c4af667f-8d65-4dc5-bc51-573af3611122",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9c1d481d-0760-4e9f-a005-af3c31815993",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": true,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 80,
    "xorig": 40,
    "yorig": 96
}