{
    "id": "70710539-6765-485d-a114-31f61330a377",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "gui_menu",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8d5160f3-01ce-4e66-8a45-d8eef17e743c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "70710539-6765-485d-a114-31f61330a377",
            "compositeImage": {
                "id": "2ffbceaa-8ca5-4f3c-a843-c6f15d319f1c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8d5160f3-01ce-4e66-8a45-d8eef17e743c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "961cad2a-d6b9-41eb-887f-132d4ac7d0a8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8d5160f3-01ce-4e66-8a45-d8eef17e743c",
                    "LayerId": "f50e3eeb-4ea2-4e79-b0c5-a0a0efb11a18"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "f50e3eeb-4ea2-4e79-b0c5-a0a0efb11a18",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "70710539-6765-485d-a114-31f61330a377",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}