{
    "id": "7d608b0f-af5a-4183-a549-73ee5c0d4133",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "ui_pointer",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 55,
    "bbox_left": 8,
    "bbox_right": 62,
    "bbox_top": 8,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ec5da88a-533f-43c5-b03c-e9000f337ad5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7d608b0f-af5a-4183-a549-73ee5c0d4133",
            "compositeImage": {
                "id": "4d118ef6-babf-4825-b857-f6c31e957bff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ec5da88a-533f-43c5-b03c-e9000f337ad5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4f320d20-9054-401d-a7fb-cdc583b74a50",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ec5da88a-533f-43c5-b03c-e9000f337ad5",
                    "LayerId": "6b701e64-a991-43ce-abe9-5e6a5a632b70"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "6b701e64-a991-43ce-abe9-5e6a5a632b70",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7d608b0f-af5a-4183-a549-73ee5c0d4133",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}