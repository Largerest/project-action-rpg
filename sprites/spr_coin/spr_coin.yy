{
    "id": "bfd49cea-e79c-4bd1-9d1e-2b3488ced258",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_coin",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 5,
    "bbox_left": 0,
    "bbox_right": 5,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7631474e-6cb0-42f7-aaf3-179b790d75be",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bfd49cea-e79c-4bd1-9d1e-2b3488ced258",
            "compositeImage": {
                "id": "ba89ebcb-0d1a-4d2c-9e3f-2c2cfd3e3e10",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7631474e-6cb0-42f7-aaf3-179b790d75be",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5983df57-3812-4694-a125-abc8f6e3f833",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7631474e-6cb0-42f7-aaf3-179b790d75be",
                    "LayerId": "364e75f0-73be-4f59-9eed-f8c3f48bb8c3"
                }
            ]
        },
        {
            "id": "af724096-0f21-479f-9793-f97f102cae52",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bfd49cea-e79c-4bd1-9d1e-2b3488ced258",
            "compositeImage": {
                "id": "07c2cbb5-6679-4e5e-b833-27a1f73c0d72",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "af724096-0f21-479f-9793-f97f102cae52",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1aea056f-4c22-418f-9f1c-db2f61063862",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "af724096-0f21-479f-9793-f97f102cae52",
                    "LayerId": "364e75f0-73be-4f59-9eed-f8c3f48bb8c3"
                }
            ]
        },
        {
            "id": "a9c859f0-ab5f-496e-a6e6-b1ecf86e688a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bfd49cea-e79c-4bd1-9d1e-2b3488ced258",
            "compositeImage": {
                "id": "dcec28f0-d569-46cb-9d56-16fce140df94",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a9c859f0-ab5f-496e-a6e6-b1ecf86e688a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6f9009cb-af81-4404-abd5-cdaed1b925ee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a9c859f0-ab5f-496e-a6e6-b1ecf86e688a",
                    "LayerId": "364e75f0-73be-4f59-9eed-f8c3f48bb8c3"
                }
            ]
        },
        {
            "id": "4e892cac-fd26-4d7f-8e46-5a4a9d5be0c1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bfd49cea-e79c-4bd1-9d1e-2b3488ced258",
            "compositeImage": {
                "id": "57680483-4e1a-4b0b-9d52-fa22a03420ad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4e892cac-fd26-4d7f-8e46-5a4a9d5be0c1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8da8bb30-ccf2-44e4-9746-8cc5f4d8bc01",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4e892cac-fd26-4d7f-8e46-5a4a9d5be0c1",
                    "LayerId": "364e75f0-73be-4f59-9eed-f8c3f48bb8c3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 6,
    "layers": [
        {
            "id": "364e75f0-73be-4f59-9eed-f8c3f48bb8c3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bfd49cea-e79c-4bd1-9d1e-2b3488ced258",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 6,
    "xorig": 3,
    "yorig": 6
}