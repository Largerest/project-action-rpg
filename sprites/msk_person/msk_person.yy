{
    "id": "a727deeb-e8bf-489a-b226-350119fdebc0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "msk_person",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 17,
    "bbox_left": 0,
    "bbox_right": 13,
    "bbox_top": 14,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c696ff61-42b4-471e-bddf-9e9d3155e692",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a727deeb-e8bf-489a-b226-350119fdebc0",
            "compositeImage": {
                "id": "dd99c9cf-1e7f-40ab-abcc-0eba12610743",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c696ff61-42b4-471e-bddf-9e9d3155e692",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4b5dac3b-2f60-4902-abf5-11044aea9c37",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c696ff61-42b4-471e-bddf-9e9d3155e692",
                    "LayerId": "64b1e89b-0ba0-4382-9117-5a2a35b3b483"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 18,
    "layers": [
        {
            "id": "64b1e89b-0ba0-4382-9117-5a2a35b3b483",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a727deeb-e8bf-489a-b226-350119fdebc0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": true,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 14,
    "xorig": 7,
    "yorig": 16
}