{
    "id": "44f0c36b-6f1d-485c-85f7-705178c7f116",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "gfx_explosion",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 41,
    "bbox_left": 0,
    "bbox_right": 49,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b32904a5-f380-4125-b19b-376b4bf7ed23",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "44f0c36b-6f1d-485c-85f7-705178c7f116",
            "compositeImage": {
                "id": "343b9e1b-82d2-4a4f-bf6a-145eb2591b23",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b32904a5-f380-4125-b19b-376b4bf7ed23",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "166e3c8b-8499-40d9-975f-e57940819954",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b32904a5-f380-4125-b19b-376b4bf7ed23",
                    "LayerId": "b28b5e10-5fc4-4636-a779-7b7ecaf813c9"
                }
            ]
        },
        {
            "id": "d1eaa67c-f642-4c73-9bd1-a089ade58d26",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "44f0c36b-6f1d-485c-85f7-705178c7f116",
            "compositeImage": {
                "id": "c5d228a7-d6d7-4ad2-8a9c-4ebdeeffa9e2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d1eaa67c-f642-4c73-9bd1-a089ade58d26",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "af441010-7df1-4360-a062-95304ff99e8c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d1eaa67c-f642-4c73-9bd1-a089ade58d26",
                    "LayerId": "b28b5e10-5fc4-4636-a779-7b7ecaf813c9"
                }
            ]
        },
        {
            "id": "086e05f2-cf29-4cbd-8eb2-2c81718c1734",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "44f0c36b-6f1d-485c-85f7-705178c7f116",
            "compositeImage": {
                "id": "aa5efa52-f52d-4564-94ca-2695c9c520ad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "086e05f2-cf29-4cbd-8eb2-2c81718c1734",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c1e4afee-2324-4a27-9e35-df9c35851a80",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "086e05f2-cf29-4cbd-8eb2-2c81718c1734",
                    "LayerId": "b28b5e10-5fc4-4636-a779-7b7ecaf813c9"
                }
            ]
        },
        {
            "id": "f957a176-0815-465b-bd2e-f5ae8eb4d876",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "44f0c36b-6f1d-485c-85f7-705178c7f116",
            "compositeImage": {
                "id": "31cf1396-630c-4969-b3fc-ba460d7e7862",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f957a176-0815-465b-bd2e-f5ae8eb4d876",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "788c0dfb-3800-477a-a085-4e188e964e73",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f957a176-0815-465b-bd2e-f5ae8eb4d876",
                    "LayerId": "b28b5e10-5fc4-4636-a779-7b7ecaf813c9"
                }
            ]
        },
        {
            "id": "c71ad29e-4192-4ad1-a736-0aca76a4a934",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "44f0c36b-6f1d-485c-85f7-705178c7f116",
            "compositeImage": {
                "id": "5c375a02-5982-4d38-9c36-1e06d5791d79",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c71ad29e-4192-4ad1-a736-0aca76a4a934",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "096e3f98-0b61-4c7f-888d-8c60e52c3f21",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c71ad29e-4192-4ad1-a736-0aca76a4a934",
                    "LayerId": "b28b5e10-5fc4-4636-a779-7b7ecaf813c9"
                }
            ]
        },
        {
            "id": "551c1b22-a045-41f2-b2b0-2256102dec1b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "44f0c36b-6f1d-485c-85f7-705178c7f116",
            "compositeImage": {
                "id": "39ae863e-155b-43c4-b145-9d09786053cb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "551c1b22-a045-41f2-b2b0-2256102dec1b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bdada3d0-c707-4a80-9e28-fff3455bfec8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "551c1b22-a045-41f2-b2b0-2256102dec1b",
                    "LayerId": "b28b5e10-5fc4-4636-a779-7b7ecaf813c9"
                }
            ]
        },
        {
            "id": "5e375d02-6e1b-4cfd-a331-535fd7e3fc25",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "44f0c36b-6f1d-485c-85f7-705178c7f116",
            "compositeImage": {
                "id": "886d658c-1ee6-40ad-b1e2-25a1ca40f9a4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5e375d02-6e1b-4cfd-a331-535fd7e3fc25",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6815ea98-0750-483b-a352-a8bdaed673a5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e375d02-6e1b-4cfd-a331-535fd7e3fc25",
                    "LayerId": "b28b5e10-5fc4-4636-a779-7b7ecaf813c9"
                }
            ]
        },
        {
            "id": "a101cd81-584b-4ca8-a78e-8c7eb8da2c97",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "44f0c36b-6f1d-485c-85f7-705178c7f116",
            "compositeImage": {
                "id": "c447a1b0-9d62-484e-88bf-ef5c642e5ea4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a101cd81-584b-4ca8-a78e-8c7eb8da2c97",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8ef0e1c8-0226-4205-b144-475ca2f4b206",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a101cd81-584b-4ca8-a78e-8c7eb8da2c97",
                    "LayerId": "b28b5e10-5fc4-4636-a779-7b7ecaf813c9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 42,
    "layers": [
        {
            "id": "b28b5e10-5fc4-4636-a779-7b7ecaf813c9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "44f0c36b-6f1d-485c-85f7-705178c7f116",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 50,
    "xorig": 26,
    "yorig": 19
}