{
    "id": "babf9ef0-6d96-4010-88ce-1ba0aea9a5d6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "gui_system",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "036c1c63-f719-41a2-8b41-093b9c0874d4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "babf9ef0-6d96-4010-88ce-1ba0aea9a5d6",
            "compositeImage": {
                "id": "919fbdb7-73ad-4c6b-9916-ee65328ac91a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "036c1c63-f719-41a2-8b41-093b9c0874d4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "da0fca89-0af4-40c1-80b5-385d26b9ded5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "036c1c63-f719-41a2-8b41-093b9c0874d4",
                    "LayerId": "f60e916e-310a-45f2-84fb-78a81ace475d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "f60e916e-310a-45f2-84fb-78a81ace475d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "babf9ef0-6d96-4010-88ce-1ba0aea9a5d6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}