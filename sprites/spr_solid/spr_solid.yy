{
    "id": "1201a107-eaf7-444f-81db-db2d584827c5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_solid",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d9e2ae68-83cc-41d8-8367-75564b9a71d5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1201a107-eaf7-444f-81db-db2d584827c5",
            "compositeImage": {
                "id": "1345d5f8-f313-44d2-b4ba-1b51dbafea4a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d9e2ae68-83cc-41d8-8367-75564b9a71d5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2036e95b-16c7-4df8-a91c-5f2ebe649721",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d9e2ae68-83cc-41d8-8367-75564b9a71d5",
                    "LayerId": "1e3324bf-c03f-49ec-b0a6-2d62e3110d78"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "1e3324bf-c03f-49ec-b0a6-2d62e3110d78",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1201a107-eaf7-444f-81db-db2d584827c5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}