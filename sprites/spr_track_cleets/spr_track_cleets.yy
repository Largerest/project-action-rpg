{
    "id": "68200385-a467-48bc-aff5-8775f11e723b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_track_cleets",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9ec115c5-05e9-43c6-9569-458cd3708b08",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "68200385-a467-48bc-aff5-8775f11e723b",
            "compositeImage": {
                "id": "4f05c023-f635-4b86-8a2e-e8c4ff2d3b85",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9ec115c5-05e9-43c6-9569-458cd3708b08",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cb76c9d5-9a06-4f6c-ab2f-992cf15190fd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9ec115c5-05e9-43c6-9569-458cd3708b08",
                    "LayerId": "e45e724e-9676-4a36-8909-08eaeab6a8fd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "e45e724e-9676-4a36-8909-08eaeab6a8fd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "68200385-a467-48bc-aff5-8775f11e723b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}