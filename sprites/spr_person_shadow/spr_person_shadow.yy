{
    "id": "5bcb4485-c717-4a44-add1-d7d7f746fcca",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_person_shadow",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 3,
    "bbox_left": 0,
    "bbox_right": 10,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d780627d-3cd7-4494-bb6b-c0923df563cf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5bcb4485-c717-4a44-add1-d7d7f746fcca",
            "compositeImage": {
                "id": "4fdf679d-c475-41a7-8b5e-1399fe605c17",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d780627d-3cd7-4494-bb6b-c0923df563cf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3db186cb-9cfc-4ec9-ab21-631b5833ea28",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d780627d-3cd7-4494-bb6b-c0923df563cf",
                    "LayerId": "e16b48d1-d579-4b90-a7fe-fb58b88b98d2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 4,
    "layers": [
        {
            "id": "e16b48d1-d579-4b90-a7fe-fb58b88b98d2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5bcb4485-c717-4a44-add1-d7d7f746fcca",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 11,
    "xorig": 5,
    "yorig": 2
}