{
    "id": "39b5df00-cca1-4f43-b8e1-a348497258b1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_dollar",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "43517909-b3b1-4f92-983b-c77d417095fc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "39b5df00-cca1-4f43-b8e1-a348497258b1",
            "compositeImage": {
                "id": "94b86eef-e9f7-4757-a670-835ffe5270eb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "43517909-b3b1-4f92-983b-c77d417095fc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7effaee8-b3ca-4a66-9db3-33386b85651d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "43517909-b3b1-4f92-983b-c77d417095fc",
                    "LayerId": "c62d367e-a68c-4fe3-bd7a-288ec6b91028"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "c62d367e-a68c-4fe3-bd7a-288ec6b91028",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "39b5df00-cca1-4f43-b8e1-a348497258b1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}