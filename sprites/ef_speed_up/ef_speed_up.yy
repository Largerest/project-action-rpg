{
    "id": "25e84aa0-84f0-4d3e-ab37-0c031d5e5f7f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "ef_speed_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 3,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7d6810f5-70c1-4630-b945-0c77bad039bc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "25e84aa0-84f0-4d3e-ab37-0c031d5e5f7f",
            "compositeImage": {
                "id": "602607ae-7462-40b4-9a18-0dc524277ac3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7d6810f5-70c1-4630-b945-0c77bad039bc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d4270150-29e0-40bc-b7b0-29c1c72a0b92",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7d6810f5-70c1-4630-b945-0c77bad039bc",
                    "LayerId": "fea29d21-daf7-4d6f-bc54-e416b9447279"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "fea29d21-daf7-4d6f-bc54-e416b9447279",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "25e84aa0-84f0-4d3e-ab37-0c031d5e5f7f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 4,
    "xorig": 2,
    "yorig": 4
}