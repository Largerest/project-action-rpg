{
    "id": "01252040-fbc4-45df-99df-75d417cad599",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "ef_dust",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 3,
    "bbox_left": 0,
    "bbox_right": 3,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "860ee415-5534-4810-988b-2d19dbe2c7c1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "01252040-fbc4-45df-99df-75d417cad599",
            "compositeImage": {
                "id": "b0a8fd80-f1fc-4601-b831-4493c1270e10",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "860ee415-5534-4810-988b-2d19dbe2c7c1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1f7af8e6-0dd5-4a01-88ac-9c4cb49766ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "860ee415-5534-4810-988b-2d19dbe2c7c1",
                    "LayerId": "5ed4c7ae-fa92-463b-8546-647b3398dc80"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 4,
    "layers": [
        {
            "id": "5ed4c7ae-fa92-463b-8546-647b3398dc80",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "01252040-fbc4-45df-99df-75d417cad599",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 4,
    "xorig": 2,
    "yorig": 2
}