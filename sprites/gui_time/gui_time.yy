{
    "id": "047749e8-b121-4b95-a221-8f499435be25",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "gui_time",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c72f4c54-59f4-4a72-9dc2-0bff4994d88d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "047749e8-b121-4b95-a221-8f499435be25",
            "compositeImage": {
                "id": "924062d2-4e70-4f2e-ac99-ddcd743674bd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c72f4c54-59f4-4a72-9dc2-0bff4994d88d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "29f3b80a-e6d9-43ca-b4fc-dadd012d50c8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c72f4c54-59f4-4a72-9dc2-0bff4994d88d",
                    "LayerId": "4cee48d1-3f08-4f68-9949-dcee4c46bd74"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "4cee48d1-3f08-4f68-9949-dcee4c46bd74",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "047749e8-b121-4b95-a221-8f499435be25",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}