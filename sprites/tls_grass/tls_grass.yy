{
    "id": "4fa952d2-0c1e-4869-a73c-d80240cfe9ab",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "tls_grass",
    "For3D": false,
    "HTile": true,
    "VTile": true,
    "bbox_bottom": 47,
    "bbox_left": 7,
    "bbox_right": 95,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cad254ca-4b03-482a-9a53-cea891442ef6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4fa952d2-0c1e-4869-a73c-d80240cfe9ab",
            "compositeImage": {
                "id": "4c6cb443-e565-4720-a085-195b6b1897a4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cad254ca-4b03-482a-9a53-cea891442ef6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "70e3bcfb-052b-4606-b825-1eb14fd2d885",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cad254ca-4b03-482a-9a53-cea891442ef6",
                    "LayerId": "5deac19d-b0c4-4aef-a6d3-c708cd35baa7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "5deac19d-b0c4-4aef-a6d3-c708cd35baa7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4fa952d2-0c1e-4869-a73c-d80240cfe9ab",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 0,
    "yorig": 0
}