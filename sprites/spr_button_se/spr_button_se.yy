{
    "id": "859fc64c-0ccf-49a1-9004-1fc594a73ada",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_button_se",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 47,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d2ca29ee-bdf2-49ba-b1f7-a327081e81a8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "859fc64c-0ccf-49a1-9004-1fc594a73ada",
            "compositeImage": {
                "id": "c6abd40c-5943-4e43-8a42-c1d7acd74ff1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d2ca29ee-bdf2-49ba-b1f7-a327081e81a8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "05fab5f3-4862-4902-9a65-d7be973d650c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d2ca29ee-bdf2-49ba-b1f7-a327081e81a8",
                    "LayerId": "9c59a0b1-3a75-45fc-a158-5c1cbc7e2b35"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "9c59a0b1-3a75-45fc-a158-5c1cbc7e2b35",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "859fc64c-0ccf-49a1-9004-1fc594a73ada",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 8
}