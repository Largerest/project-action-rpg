{
    "id": "2d933de5-25bf-40c2-993a-17f777a6a2b6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_house",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 79,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c55f97da-6a7b-4e11-8868-0e806837af53",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d933de5-25bf-40c2-993a-17f777a6a2b6",
            "compositeImage": {
                "id": "374d5518-9b7b-4247-84cb-941f4209e4f7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c55f97da-6a7b-4e11-8868-0e806837af53",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b44151da-f65f-4c47-ba33-a1f5036889fd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c55f97da-6a7b-4e11-8868-0e806837af53",
                    "LayerId": "f67d2266-6f03-48a9-87b9-6d7df59f2c18"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "f67d2266-6f03-48a9-87b9-6d7df59f2c18",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2d933de5-25bf-40c2-993a-17f777a6a2b6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 80,
    "xorig": 40,
    "yorig": 96
}