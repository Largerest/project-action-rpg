{
    "id": "6c29ab89-16df-435b-afd8-58aa9fb341ca",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bullet",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 4,
    "bbox_left": 1,
    "bbox_right": 4,
    "bbox_top": 1,
    "bboxmode": 2,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f4402260-a4be-4d53-bb0f-635ddca702fc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6c29ab89-16df-435b-afd8-58aa9fb341ca",
            "compositeImage": {
                "id": "776488b8-a687-4f62-98de-82b081b69b31",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f4402260-a4be-4d53-bb0f-635ddca702fc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "52e2f3ca-b196-45ad-bfc1-db3b0843949a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f4402260-a4be-4d53-bb0f-635ddca702fc",
                    "LayerId": "5db68e53-c5bb-4b96-b090-04a69a21ce95"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 6,
    "layers": [
        {
            "id": "5db68e53-c5bb-4b96-b090-04a69a21ce95",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6c29ab89-16df-435b-afd8-58aa9fb341ca",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": true,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 6,
    "xorig": 3,
    "yorig": 3
}