{
    "id": "6df58365-1526-4a50-bac2-e7721dac896e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_lili_run",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 17,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "63832af0-00fa-486a-b90e-1369a0c0892e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6df58365-1526-4a50-bac2-e7721dac896e",
            "compositeImage": {
                "id": "eb3f2fd6-e349-4ecb-af9b-44dc81e44d44",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "63832af0-00fa-486a-b90e-1369a0c0892e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7397a25e-5ab3-4a43-8538-ba09908614a9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "63832af0-00fa-486a-b90e-1369a0c0892e",
                    "LayerId": "fa1ba8c4-b340-4415-8fbf-4e2922c1014e"
                }
            ]
        },
        {
            "id": "c4ec8d29-323a-4531-be8c-b53a28134af5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6df58365-1526-4a50-bac2-e7721dac896e",
            "compositeImage": {
                "id": "cf915546-92fd-4cae-9dc1-8cbc188106c1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c4ec8d29-323a-4531-be8c-b53a28134af5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5931a841-7f9a-449a-bd61-2ac04af665ed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c4ec8d29-323a-4531-be8c-b53a28134af5",
                    "LayerId": "fa1ba8c4-b340-4415-8fbf-4e2922c1014e"
                }
            ]
        },
        {
            "id": "347c2580-72af-41a5-8819-a2363aed7e17",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6df58365-1526-4a50-bac2-e7721dac896e",
            "compositeImage": {
                "id": "015b8841-559e-4591-9e61-b60b239426dc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "347c2580-72af-41a5-8819-a2363aed7e17",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7fb368d8-a29f-461a-8ec2-94802be65afb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "347c2580-72af-41a5-8819-a2363aed7e17",
                    "LayerId": "fa1ba8c4-b340-4415-8fbf-4e2922c1014e"
                }
            ]
        },
        {
            "id": "4aac1f67-c4cb-4f33-b4d8-6640a0085037",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6df58365-1526-4a50-bac2-e7721dac896e",
            "compositeImage": {
                "id": "c9c94a9a-4e32-45ba-94c3-c0984bb00a53",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4aac1f67-c4cb-4f33-b4d8-6640a0085037",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "adae2131-083c-4d2e-8ab9-a210796d01fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4aac1f67-c4cb-4f33-b4d8-6640a0085037",
                    "LayerId": "fa1ba8c4-b340-4415-8fbf-4e2922c1014e"
                }
            ]
        },
        {
            "id": "74fbe066-ec42-4c0e-8890-295dff9cf055",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6df58365-1526-4a50-bac2-e7721dac896e",
            "compositeImage": {
                "id": "d10874c7-e3b6-4109-9338-bb2a79ceac5b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "74fbe066-ec42-4c0e-8890-295dff9cf055",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b3476828-8ed1-45c2-8c5c-d98401db1f62",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "74fbe066-ec42-4c0e-8890-295dff9cf055",
                    "LayerId": "fa1ba8c4-b340-4415-8fbf-4e2922c1014e"
                }
            ]
        },
        {
            "id": "93e6ed14-c09c-40df-87c6-91637ad496b7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6df58365-1526-4a50-bac2-e7721dac896e",
            "compositeImage": {
                "id": "2be27c9d-3c07-4996-ba23-f0c72d349ee9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "93e6ed14-c09c-40df-87c6-91637ad496b7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "238ca6c0-ab8f-4ede-98f2-50013a35652c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "93e6ed14-c09c-40df-87c6-91637ad496b7",
                    "LayerId": "fa1ba8c4-b340-4415-8fbf-4e2922c1014e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 18,
    "layers": [
        {
            "id": "fa1ba8c4-b340-4415-8fbf-4e2922c1014e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6df58365-1526-4a50-bac2-e7721dac896e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 9,
    "yorig": 18
}