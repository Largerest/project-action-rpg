{
    "id": "e7838b94-f9d2-4cdf-8451-5b12179fab60",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_lili_idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 17,
    "bbox_left": 0,
    "bbox_right": 13,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d227cf91-f9f8-4b6f-883d-67a42b45901a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e7838b94-f9d2-4cdf-8451-5b12179fab60",
            "compositeImage": {
                "id": "13f13396-9562-468f-8006-0f2fdda33db3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d227cf91-f9f8-4b6f-883d-67a42b45901a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5bc3433f-61ea-48bc-b5ab-ffa499ee3866",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d227cf91-f9f8-4b6f-883d-67a42b45901a",
                    "LayerId": "ce40ec97-33a3-4e13-b038-b21728b20020"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 18,
    "layers": [
        {
            "id": "ce40ec97-33a3-4e13-b038-b21728b20020",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e7838b94-f9d2-4cdf-8451-5b12179fab60",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 9,
    "yorig": 18
}