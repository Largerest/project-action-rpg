{
    "id": "bbee54ef-f7b8-4fb4-bf00-9f93fce38262",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "gui_controls",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "46a7a640-bd3e-492f-8bd5-dfb537097a9e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bbee54ef-f7b8-4fb4-bf00-9f93fce38262",
            "compositeImage": {
                "id": "a1954a1e-ebad-4c50-ac00-9647709f31da",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "46a7a640-bd3e-492f-8bd5-dfb537097a9e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "63d4e015-c2ec-43de-807c-ec9bf8d2d4cc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "46a7a640-bd3e-492f-8bd5-dfb537097a9e",
                    "LayerId": "05dd12be-fbce-4440-9a20-ec830929d1b8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "05dd12be-fbce-4440-9a20-ec830929d1b8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bbee54ef-f7b8-4fb4-bf00-9f93fce38262",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}