{
    "id": "ef613300-00d9-4125-a1cb-abff96e92de9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "gfx_flicker",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0d655148-9999-4f8f-8066-cffe6be5f41f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ef613300-00d9-4125-a1cb-abff96e92de9",
            "compositeImage": {
                "id": "0ef38b20-361c-4f42-abfa-8d3fef826a4d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0d655148-9999-4f8f-8066-cffe6be5f41f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "87ce494c-97ca-4dd2-bb97-c96ef07b3b50",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0d655148-9999-4f8f-8066-cffe6be5f41f",
                    "LayerId": "9e631560-517b-4b89-8ea0-833bc9ad7176"
                }
            ]
        },
        {
            "id": "e08eabf9-c10b-4f3a-9e2e-8db1e1d18bc0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ef613300-00d9-4125-a1cb-abff96e92de9",
            "compositeImage": {
                "id": "8df6d605-13d3-428b-a670-267afa0e5385",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e08eabf9-c10b-4f3a-9e2e-8db1e1d18bc0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "17286503-a76a-49ec-852e-e66eb6e1285f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e08eabf9-c10b-4f3a-9e2e-8db1e1d18bc0",
                    "LayerId": "9e631560-517b-4b89-8ea0-833bc9ad7176"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "9e631560-517b-4b89-8ea0-833bc9ad7176",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ef613300-00d9-4125-a1cb-abff96e92de9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}