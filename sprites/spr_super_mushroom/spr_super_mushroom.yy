{
    "id": "827d0105-fdb4-4e6e-aabc-37d013293dca",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_super_mushroom",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 17,
    "bbox_left": 0,
    "bbox_right": 17,
    "bbox_top": 14,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9d51a271-6e61-4cb5-9dfa-2247fe352a1c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "827d0105-fdb4-4e6e-aabc-37d013293dca",
            "compositeImage": {
                "id": "474f0de4-3f9a-4fb5-beeb-36f9b7fbaeab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9d51a271-6e61-4cb5-9dfa-2247fe352a1c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b08c18af-2911-4707-83ef-95b360a0b89f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9d51a271-6e61-4cb5-9dfa-2247fe352a1c",
                    "LayerId": "6b1bde9f-8a0e-4c0d-9be8-f454c547c435"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 18,
    "layers": [
        {
            "id": "6b1bde9f-8a0e-4c0d-9be8-f454c547c435",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "827d0105-fdb4-4e6e-aabc-37d013293dca",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 18,
    "xorig": 9,
    "yorig": 18
}