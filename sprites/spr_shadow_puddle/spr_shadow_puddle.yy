{
    "id": "285155b0-6f9e-4dd5-8326-cbd6a1bd73de",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_shadow_puddle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 12,
    "bbox_left": 0,
    "bbox_right": 38,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d6729b23-cb13-4fab-9cbf-1074e9e52cc6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "285155b0-6f9e-4dd5-8326-cbd6a1bd73de",
            "compositeImage": {
                "id": "60ed7b5f-1c5a-4aa8-ac00-97ee58a3eedf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d6729b23-cb13-4fab-9cbf-1074e9e52cc6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "da9b78da-c6bb-4fa0-9cee-4f36065620d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d6729b23-cb13-4fab-9cbf-1074e9e52cc6",
                    "LayerId": "d405e65c-c4c0-4021-85dd-af2220b8076a"
                }
            ]
        },
        {
            "id": "0bdf45c2-3c3e-4cd8-a951-e0c45474aac7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "285155b0-6f9e-4dd5-8326-cbd6a1bd73de",
            "compositeImage": {
                "id": "33cd63e1-dfb8-49ba-8333-8cef6cba46b7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0bdf45c2-3c3e-4cd8-a951-e0c45474aac7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1c7b4538-edd9-40ee-9a36-f33a72217540",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0bdf45c2-3c3e-4cd8-a951-e0c45474aac7",
                    "LayerId": "d405e65c-c4c0-4021-85dd-af2220b8076a"
                }
            ]
        },
        {
            "id": "f5e8ff6d-3901-444f-b40b-3fb83e390150",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "285155b0-6f9e-4dd5-8326-cbd6a1bd73de",
            "compositeImage": {
                "id": "ae1a99eb-a835-4505-9f86-0b382c04f53f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f5e8ff6d-3901-444f-b40b-3fb83e390150",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "34587bd9-b09f-4d3e-bbca-57e17a5e8d54",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f5e8ff6d-3901-444f-b40b-3fb83e390150",
                    "LayerId": "d405e65c-c4c0-4021-85dd-af2220b8076a"
                }
            ]
        },
        {
            "id": "c4ba06b6-1289-455d-9eeb-f3e7ef8ef59b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "285155b0-6f9e-4dd5-8326-cbd6a1bd73de",
            "compositeImage": {
                "id": "a3052eab-a883-4eed-8ce7-83a244883619",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c4ba06b6-1289-455d-9eeb-f3e7ef8ef59b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8f529866-863c-482d-9a83-11692445465e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c4ba06b6-1289-455d-9eeb-f3e7ef8ef59b",
                    "LayerId": "d405e65c-c4c0-4021-85dd-af2220b8076a"
                }
            ]
        },
        {
            "id": "fc15ecde-fa9d-4ac3-a5a6-7d5e66353c03",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "285155b0-6f9e-4dd5-8326-cbd6a1bd73de",
            "compositeImage": {
                "id": "247166b8-b930-45b1-82d1-bd643f26f521",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fc15ecde-fa9d-4ac3-a5a6-7d5e66353c03",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bd7efac6-036f-4e6f-a33c-4be742a88693",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc15ecde-fa9d-4ac3-a5a6-7d5e66353c03",
                    "LayerId": "d405e65c-c4c0-4021-85dd-af2220b8076a"
                }
            ]
        },
        {
            "id": "8b373ef4-dfe0-463f-a028-737bb8252b33",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "285155b0-6f9e-4dd5-8326-cbd6a1bd73de",
            "compositeImage": {
                "id": "91b09e15-dd96-4ce3-94a1-2a9fdbf8d3b7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8b373ef4-dfe0-463f-a028-737bb8252b33",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eea06d3a-7a03-4a3d-be4d-2724d6bce312",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8b373ef4-dfe0-463f-a028-737bb8252b33",
                    "LayerId": "d405e65c-c4c0-4021-85dd-af2220b8076a"
                }
            ]
        },
        {
            "id": "315db1b0-0df4-468c-8e0d-323199fbeb09",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "285155b0-6f9e-4dd5-8326-cbd6a1bd73de",
            "compositeImage": {
                "id": "f5839263-4c25-4ebf-b113-71e0255d3556",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "315db1b0-0df4-468c-8e0d-323199fbeb09",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c35310d8-afae-4adf-8d4a-fd7b5503c756",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "315db1b0-0df4-468c-8e0d-323199fbeb09",
                    "LayerId": "d405e65c-c4c0-4021-85dd-af2220b8076a"
                }
            ]
        },
        {
            "id": "5f9b729d-d95e-4fd5-8314-b7cb83908861",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "285155b0-6f9e-4dd5-8326-cbd6a1bd73de",
            "compositeImage": {
                "id": "6882848b-a146-4de5-b5c9-58b18e837df6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5f9b729d-d95e-4fd5-8314-b7cb83908861",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "84b7bb30-9b3d-4eca-807f-0c3072cea20a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5f9b729d-d95e-4fd5-8314-b7cb83908861",
                    "LayerId": "d405e65c-c4c0-4021-85dd-af2220b8076a"
                }
            ]
        },
        {
            "id": "da161515-0ef4-4ef3-b43b-951c05e4fd9c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "285155b0-6f9e-4dd5-8326-cbd6a1bd73de",
            "compositeImage": {
                "id": "80efe63e-78df-4322-a9c4-e77e6cb6b494",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "da161515-0ef4-4ef3-b43b-951c05e4fd9c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca873405-ccfa-4e2c-a50a-edf9bb589dc0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "da161515-0ef4-4ef3-b43b-951c05e4fd9c",
                    "LayerId": "d405e65c-c4c0-4021-85dd-af2220b8076a"
                }
            ]
        },
        {
            "id": "be8e905d-f93a-4ede-8d35-4b01fe9c1804",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "285155b0-6f9e-4dd5-8326-cbd6a1bd73de",
            "compositeImage": {
                "id": "a3d215c9-16f9-48df-af23-9d01d8fd9c10",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "be8e905d-f93a-4ede-8d35-4b01fe9c1804",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ce145b59-0044-4f88-a352-af565538bc7b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "be8e905d-f93a-4ede-8d35-4b01fe9c1804",
                    "LayerId": "d405e65c-c4c0-4021-85dd-af2220b8076a"
                }
            ]
        },
        {
            "id": "ebba89d6-6710-4c76-8008-af0f13517ef6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "285155b0-6f9e-4dd5-8326-cbd6a1bd73de",
            "compositeImage": {
                "id": "4bdfa5c2-679c-46f2-9daa-642f3647015a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ebba89d6-6710-4c76-8008-af0f13517ef6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1d3d080e-51e1-4b0f-baf4-70e5f852967e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ebba89d6-6710-4c76-8008-af0f13517ef6",
                    "LayerId": "d405e65c-c4c0-4021-85dd-af2220b8076a"
                }
            ]
        },
        {
            "id": "3ab2a185-9849-4c7b-a683-2041a3cee84b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "285155b0-6f9e-4dd5-8326-cbd6a1bd73de",
            "compositeImage": {
                "id": "ded38656-d672-4048-bb77-7fa952f7a532",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3ab2a185-9849-4c7b-a683-2041a3cee84b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "228cd4a4-5820-4c8d-8a19-d1846e6a5e0b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3ab2a185-9849-4c7b-a683-2041a3cee84b",
                    "LayerId": "d405e65c-c4c0-4021-85dd-af2220b8076a"
                }
            ]
        },
        {
            "id": "f6d09947-bbeb-4c20-8357-8354e2ae069a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "285155b0-6f9e-4dd5-8326-cbd6a1bd73de",
            "compositeImage": {
                "id": "27f99863-fa32-495c-bb5c-17686ddcdf7a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f6d09947-bbeb-4c20-8357-8354e2ae069a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0eb22279-b3ef-4033-bc89-41660fcc0f89",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f6d09947-bbeb-4c20-8357-8354e2ae069a",
                    "LayerId": "d405e65c-c4c0-4021-85dd-af2220b8076a"
                }
            ]
        },
        {
            "id": "e327cef2-a20a-4443-9f76-da74613b8d77",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "285155b0-6f9e-4dd5-8326-cbd6a1bd73de",
            "compositeImage": {
                "id": "ff092e89-6541-49fc-8f6f-fe9197663f48",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e327cef2-a20a-4443-9f76-da74613b8d77",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5aaa8793-3355-4f4d-8248-cb78f8625fbe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e327cef2-a20a-4443-9f76-da74613b8d77",
                    "LayerId": "d405e65c-c4c0-4021-85dd-af2220b8076a"
                }
            ]
        },
        {
            "id": "c4e778cc-baa3-487b-b751-b2527632a2ba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "285155b0-6f9e-4dd5-8326-cbd6a1bd73de",
            "compositeImage": {
                "id": "fe99ad40-1835-4a91-8c5e-fe5e3040b648",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c4e778cc-baa3-487b-b751-b2527632a2ba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ecc92259-4f94-4a75-9f10-bf6a9a3d3c8c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c4e778cc-baa3-487b-b751-b2527632a2ba",
                    "LayerId": "d405e65c-c4c0-4021-85dd-af2220b8076a"
                }
            ]
        },
        {
            "id": "a5e0f4dc-16cc-4f3c-bf97-af8f98ebb44a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "285155b0-6f9e-4dd5-8326-cbd6a1bd73de",
            "compositeImage": {
                "id": "bd0515d3-b1ff-4b7a-83c4-be498f8f695b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a5e0f4dc-16cc-4f3c-bf97-af8f98ebb44a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc36ee87-a818-4912-912b-8fd7f13bd7a6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a5e0f4dc-16cc-4f3c-bf97-af8f98ebb44a",
                    "LayerId": "d405e65c-c4c0-4021-85dd-af2220b8076a"
                }
            ]
        },
        {
            "id": "cce6ed0d-6a65-4825-b584-980db51f2407",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "285155b0-6f9e-4dd5-8326-cbd6a1bd73de",
            "compositeImage": {
                "id": "1d819f4e-7936-4e67-a86c-98cc5a68c80d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cce6ed0d-6a65-4825-b584-980db51f2407",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f51fe51f-baaf-470e-9e06-00140cc11305",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cce6ed0d-6a65-4825-b584-980db51f2407",
                    "LayerId": "d405e65c-c4c0-4021-85dd-af2220b8076a"
                }
            ]
        },
        {
            "id": "2f272505-5e02-40c8-9b72-b27a4968e9a3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "285155b0-6f9e-4dd5-8326-cbd6a1bd73de",
            "compositeImage": {
                "id": "a023808e-7015-4608-bcd6-d1f1e986b174",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2f272505-5e02-40c8-9b72-b27a4968e9a3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cd28afd2-210b-4fb0-8624-042e72ce817b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2f272505-5e02-40c8-9b72-b27a4968e9a3",
                    "LayerId": "d405e65c-c4c0-4021-85dd-af2220b8076a"
                }
            ]
        },
        {
            "id": "7633c494-2309-4517-a204-c3f58dd6b4f5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "285155b0-6f9e-4dd5-8326-cbd6a1bd73de",
            "compositeImage": {
                "id": "340f2198-4f21-4264-9a47-9896ee1ed666",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7633c494-2309-4517-a204-c3f58dd6b4f5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "78b3fecf-fb8f-45f3-add9-9c7d85332bc6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7633c494-2309-4517-a204-c3f58dd6b4f5",
                    "LayerId": "d405e65c-c4c0-4021-85dd-af2220b8076a"
                }
            ]
        },
        {
            "id": "4c595582-2664-436a-aa4c-1ded184bfb9d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "285155b0-6f9e-4dd5-8326-cbd6a1bd73de",
            "compositeImage": {
                "id": "b14e774b-6d2f-4858-ab87-c3d8be45e05b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4c595582-2664-436a-aa4c-1ded184bfb9d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6d492704-b085-4a3a-89f5-22683b18fc04",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4c595582-2664-436a-aa4c-1ded184bfb9d",
                    "LayerId": "d405e65c-c4c0-4021-85dd-af2220b8076a"
                }
            ]
        },
        {
            "id": "9bf8fd7e-dc51-428b-94f0-993b2aa0df94",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "285155b0-6f9e-4dd5-8326-cbd6a1bd73de",
            "compositeImage": {
                "id": "b6501a15-6d6f-4b6e-b674-7686627d1935",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9bf8fd7e-dc51-428b-94f0-993b2aa0df94",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "41ef88e8-4894-4829-b2ce-445d741e23e6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9bf8fd7e-dc51-428b-94f0-993b2aa0df94",
                    "LayerId": "d405e65c-c4c0-4021-85dd-af2220b8076a"
                }
            ]
        },
        {
            "id": "9da20805-66df-4de3-9f86-b5be1e7ee484",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "285155b0-6f9e-4dd5-8326-cbd6a1bd73de",
            "compositeImage": {
                "id": "5619b5d2-824c-4e14-a57a-20256a373786",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9da20805-66df-4de3-9f86-b5be1e7ee484",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f50c49ac-6612-4120-80a5-6d52f4785d03",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9da20805-66df-4de3-9f86-b5be1e7ee484",
                    "LayerId": "d405e65c-c4c0-4021-85dd-af2220b8076a"
                }
            ]
        },
        {
            "id": "cdc99fee-23bd-4036-a0b4-663b2c3e1aea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "285155b0-6f9e-4dd5-8326-cbd6a1bd73de",
            "compositeImage": {
                "id": "d21a8334-b3d3-4d85-bf2c-c3fd13163d98",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cdc99fee-23bd-4036-a0b4-663b2c3e1aea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "903b49c3-d175-4219-a1e4-536ecf767796",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cdc99fee-23bd-4036-a0b4-663b2c3e1aea",
                    "LayerId": "d405e65c-c4c0-4021-85dd-af2220b8076a"
                }
            ]
        },
        {
            "id": "50b19237-d452-4adc-ba36-2246dcece92e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "285155b0-6f9e-4dd5-8326-cbd6a1bd73de",
            "compositeImage": {
                "id": "d615b7a3-85a5-49e8-8aa0-1c96aa1a85e2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "50b19237-d452-4adc-ba36-2246dcece92e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "efb1fbc1-07ee-4a8c-96b8-a28199aa771e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "50b19237-d452-4adc-ba36-2246dcece92e",
                    "LayerId": "d405e65c-c4c0-4021-85dd-af2220b8076a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 13,
    "layers": [
        {
            "id": "d405e65c-c4c0-4021-85dd-af2220b8076a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "285155b0-6f9e-4dd5-8326-cbd6a1bd73de",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 39,
    "xorig": 19,
    "yorig": 9
}