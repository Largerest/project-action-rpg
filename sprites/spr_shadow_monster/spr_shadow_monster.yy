{
    "id": "6418ffe6-3055-4ecc-83b3-1b39c5230237",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_shadow_monster",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 19,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2a0719f2-9699-4b83-980a-c1d54f25ad46",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6418ffe6-3055-4ecc-83b3-1b39c5230237",
            "compositeImage": {
                "id": "8e607bd0-1e94-47c9-81b5-a4dfb62dd9a6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2a0719f2-9699-4b83-980a-c1d54f25ad46",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ee0a5062-4bbd-4260-8d3c-10f01f78d53f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2a0719f2-9699-4b83-980a-c1d54f25ad46",
                    "LayerId": "0ea017bd-f3cc-4408-9692-0d9796385a04"
                }
            ]
        },
        {
            "id": "8bd587df-2b67-4d62-a217-4c632382bf66",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6418ffe6-3055-4ecc-83b3-1b39c5230237",
            "compositeImage": {
                "id": "c718736d-536f-4436-b5a9-9e349b0f8113",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8bd587df-2b67-4d62-a217-4c632382bf66",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2f58bdf2-eb0c-4e5b-9d8c-f5c52cf07eb7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8bd587df-2b67-4d62-a217-4c632382bf66",
                    "LayerId": "0ea017bd-f3cc-4408-9692-0d9796385a04"
                }
            ]
        },
        {
            "id": "dc3aeaae-62bf-4c02-954c-b94791506183",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6418ffe6-3055-4ecc-83b3-1b39c5230237",
            "compositeImage": {
                "id": "82059e40-beb9-47b9-92d3-1cece1a7c95f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dc3aeaae-62bf-4c02-954c-b94791506183",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c183cc58-2d04-45ef-a008-10cb1eb0dfb3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dc3aeaae-62bf-4c02-954c-b94791506183",
                    "LayerId": "0ea017bd-f3cc-4408-9692-0d9796385a04"
                }
            ]
        },
        {
            "id": "5e148045-c1d4-4864-8df1-8bac2d6d2037",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6418ffe6-3055-4ecc-83b3-1b39c5230237",
            "compositeImage": {
                "id": "94e8465a-edb2-4d91-bd50-de861c7f7228",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5e148045-c1d4-4864-8df1-8bac2d6d2037",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ee9dad05-c825-49e0-9cf9-99de4c081971",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e148045-c1d4-4864-8df1-8bac2d6d2037",
                    "LayerId": "0ea017bd-f3cc-4408-9692-0d9796385a04"
                }
            ]
        },
        {
            "id": "48006216-38ec-4372-b3df-53b71b240777",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6418ffe6-3055-4ecc-83b3-1b39c5230237",
            "compositeImage": {
                "id": "50d9ae86-35a0-481d-8466-ac41406b2a63",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "48006216-38ec-4372-b3df-53b71b240777",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b16d3dfa-0544-4edd-9489-1a82c26f1db3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "48006216-38ec-4372-b3df-53b71b240777",
                    "LayerId": "0ea017bd-f3cc-4408-9692-0d9796385a04"
                }
            ]
        },
        {
            "id": "fb616f58-261d-495d-92cf-21ea74871401",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6418ffe6-3055-4ecc-83b3-1b39c5230237",
            "compositeImage": {
                "id": "ac07f922-9dde-471d-8694-2cc423d81f3b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fb616f58-261d-495d-92cf-21ea74871401",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5086caee-4892-4497-a262-36a534d7b712",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb616f58-261d-495d-92cf-21ea74871401",
                    "LayerId": "0ea017bd-f3cc-4408-9692-0d9796385a04"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "0ea017bd-f3cc-4408-9692-0d9796385a04",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6418ffe6-3055-4ecc-83b3-1b39c5230237",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 20,
    "xorig": 10,
    "yorig": 16
}