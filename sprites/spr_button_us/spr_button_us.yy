{
    "id": "15ab00b7-c56f-4333-86c4-1882dcb3b7b4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_button_us",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 47,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d8299a9f-1e0d-4e8c-ac53-d5b113cc162c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "15ab00b7-c56f-4333-86c4-1882dcb3b7b4",
            "compositeImage": {
                "id": "2e0bf6d0-4df7-4e82-9fd1-f45785f69f48",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d8299a9f-1e0d-4e8c-ac53-d5b113cc162c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9f6f377f-c702-4b6b-8407-133cd6649402",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d8299a9f-1e0d-4e8c-ac53-d5b113cc162c",
                    "LayerId": "774937cc-743d-462c-8358-f0de2b606230"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "774937cc-743d-462c-8358-f0de2b606230",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "15ab00b7-c56f-4333-86c4-1882dcb3b7b4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 8
}