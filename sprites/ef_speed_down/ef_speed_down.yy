{
    "id": "7152d2a9-f53a-4a19-b4ab-fbc14373b4e3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "ef_speed_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 3,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a4f7270c-caae-4182-a914-686e0998a2bf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7152d2a9-f53a-4a19-b4ab-fbc14373b4e3",
            "compositeImage": {
                "id": "821a88f5-317f-4119-bf4d-a2c745c41266",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a4f7270c-caae-4182-a914-686e0998a2bf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8f41c90c-22ae-444b-8005-89122659a7f1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a4f7270c-caae-4182-a914-686e0998a2bf",
                    "LayerId": "38b561ba-d443-45b9-8a8a-2d0db8bfb5ea"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "38b561ba-d443-45b9-8a8a-2d0db8bfb5ea",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7152d2a9-f53a-4a19-b4ab-fbc14373b4e3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 4,
    "xorig": 2,
    "yorig": 4
}