{
    "id": "015f4e06-53e0-4a6c-9df2-7f2adc8b31e7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "background2",
    "For3D": false,
    "HTile": true,
    "VTile": true,
    "bbox_bottom": 239,
    "bbox_left": 0,
    "bbox_right": 319,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fef4d880-5736-4f08-be16-c69f3378ed0c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "015f4e06-53e0-4a6c-9df2-7f2adc8b31e7",
            "compositeImage": {
                "id": "c564dcd5-a692-49a3-890e-4549935cf52e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fef4d880-5736-4f08-be16-c69f3378ed0c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ccc93440-3f11-4b3c-bb35-bb655d914ffd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fef4d880-5736-4f08-be16-c69f3378ed0c",
                    "LayerId": "2a6e08fa-8e7d-4aec-a069-c09668d2bf81"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 240,
    "layers": [
        {
            "id": "2a6e08fa-8e7d-4aec-a069-c09668d2bf81",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "015f4e06-53e0-4a6c-9df2-7f2adc8b31e7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 320,
    "xorig": 0,
    "yorig": 0
}