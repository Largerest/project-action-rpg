{
    "id": "26eeae74-6817-412b-8847-0070d0140722",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_sky_slitherer",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 9,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a5f873db-c4a3-4801-a333-be33fc702132",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "26eeae74-6817-412b-8847-0070d0140722",
            "compositeImage": {
                "id": "59b12c96-2c80-4165-aef6-e6dc5f643f7f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a5f873db-c4a3-4801-a333-be33fc702132",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6a85c73d-88bf-4ee2-a50d-d6fe4135553a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a5f873db-c4a3-4801-a333-be33fc702132",
                    "LayerId": "85976fb9-9fe6-49c9-90dd-03de1ba9f7fe"
                }
            ]
        },
        {
            "id": "116078f2-5049-476a-997c-76dbfe163e79",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "26eeae74-6817-412b-8847-0070d0140722",
            "compositeImage": {
                "id": "61e7aa2d-5c64-47a8-90ff-9cebeca02d00",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "116078f2-5049-476a-997c-76dbfe163e79",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2c94d3fd-d648-4a95-b1cd-7dd748d16871",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "116078f2-5049-476a-997c-76dbfe163e79",
                    "LayerId": "85976fb9-9fe6-49c9-90dd-03de1ba9f7fe"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 10,
    "layers": [
        {
            "id": "85976fb9-9fe6-49c9-90dd-03de1ba9f7fe",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "26eeae74-6817-412b-8847-0070d0140722",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 24,
    "yorig": 5
}