{
    "id": "3a104fd5-9b97-4e52-b63d-c17a28367f6c",
    "modelName": "GMTileSet",
    "mvc": "1.11",
    "name": "tls_dungeon_tileset",
    "auto_tile_sets": [
        
    ],
    "macroPageTiles": {
        "SerialiseData": null,
        "SerialiseHeight": 0,
        "SerialiseWidth": 0,
        "TileSerialiseData": [
            
        ]
    },
    "out_columns": 3,
    "out_tilehborder": 2,
    "out_tilevborder": 2,
    "spriteId": "28cc3005-37cf-49cc-9fb8-bc2ed4b61224",
    "sprite_no_export": false,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "tile_animation": {
        "AnimationCreationOrder": null,
        "FrameData": [
            0,
            1,
            2,
            3,
            4,
            5,
            6,
            7,
            8,
            9
        ],
        "SerialiseFrameCount": 1
    },
    "tile_animation_frames": [
        
    ],
    "tile_animation_speed": 15,
    "tile_count": 10,
    "tileheight": 16,
    "tilehsep": 0,
    "tilevsep": 0,
    "tilewidth": 16,
    "tilexoff": 0,
    "tileyoff": 0
}