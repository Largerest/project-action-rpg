{
    "id": "acf2caf1-2147-430b-8cea-c6a9f1484313",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_pixel_2",
    "AntiAlias": 0,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 1,
    "first": 0,
    "fontName": "mono 07_56",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "e377c060-a71b-4a2e-99fb-8b90c2ed7477",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 13,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "d9ad200d-5f97-40a0-accc-2541bf1ccd83",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 11,
                "offset": 1,
                "shift": 7,
                "w": 1,
                "x": 117,
                "y": 58
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "efe24dc0-f651-415d-ad42-7699a61f682d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 6,
                "offset": 1,
                "shift": 7,
                "w": 3,
                "x": 109,
                "y": 58
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "9ebe5394-2fe9-4b71-bffe-1359789cd0eb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 11,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 114,
                "y": 30
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "a0ca2442-7943-412a-9ebf-0cbac796b0c6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 12,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 54,
                "y": 2
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "b6b68817-09be-4103-8a16-ce9b7c244c95",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 12,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 11,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "1f276036-d934-4191-80a9-c1f6aa71f4c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 11,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 30,
                "y": 43
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "ee4c25c5-4a4e-4500-974d-f0edfa9b6681",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 7,
                "offset": 2,
                "shift": 7,
                "w": 1,
                "x": 6,
                "y": 73
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "954f07c6-a819-4beb-94d7-21f8929ab647",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 12,
                "offset": 2,
                "shift": 7,
                "w": 2,
                "x": 105,
                "y": 58
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "1dbe901d-4a69-408f-ac50-a8a55ad2a2c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 12,
                "offset": 1,
                "shift": 7,
                "w": 2,
                "x": 93,
                "y": 58
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "adee6a9e-03ca-4a4d-927f-8b9104ccc7cc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 10,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 106,
                "y": 43
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "a8b67a51-14cc-4be1-a504-c62d972f4fc0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 10,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 99,
                "y": 43
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "6aedce85-b365-4e4a-9c7d-306d4a6e1d8d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 13,
                "offset": 1,
                "shift": 7,
                "w": 2,
                "x": 85,
                "y": 58
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "0e973961-ad86-4668-a45d-6b7b244fee5d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 8,
                "offset": 0,
                "shift": 7,
                "w": 4,
                "x": 79,
                "y": 58
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "819ad0b1-eb70-4249-8bcf-13fc5fa03975",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 11,
                "offset": 2,
                "shift": 7,
                "w": 1,
                "x": 120,
                "y": 58
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "1cd46da3-7265-4880-a29f-c2391915af9c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 12,
                "offset": 1,
                "shift": 7,
                "w": 4,
                "x": 14,
                "y": 58
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "2a2cf75f-ece8-40ea-ab25-2ddfcae3d16e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 11,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 44,
                "y": 30
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "87719010-48cf-4403-9a45-783f06f6cc9b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 11,
                "offset": 1,
                "shift": 7,
                "w": 3,
                "x": 74,
                "y": 58
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "fe49c57c-9990-42b3-87bd-cdf6be46457f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 11,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 86,
                "y": 30
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "5739b7e2-f9a3-4b76-b50d-953895883a60",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 11,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 79,
                "y": 17
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "7a743110-fbf5-4ecb-955a-cbce0ed093eb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 11,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 96,
                "y": 2
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "b9bfc320-1bf1-4ac9-a333-1a0da4a0c707",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 11,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 114,
                "y": 17
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "a0805d12-9911-4a24-b0e1-59aefd2245d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 11,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 37,
                "y": 17
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "0d23129d-a3a3-4bc6-8509-3be26a43587d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 11,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 2,
                "y": 30
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "62b47458-b2ec-4f34-892d-24f705597f43",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 11,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 2,
                "y": 43
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "3f6944e5-643d-483f-8c48-08d8af218a7d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 11,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 58,
                "y": 30
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "f4829d04-835e-4d55-8965-9c7da64636a4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 10,
                "offset": 2,
                "shift": 7,
                "w": 1,
                "x": 123,
                "y": 58
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "f97c5f12-2626-4cf3-b952-bff6c1b627a7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 13,
                "offset": 2,
                "shift": 7,
                "w": 2,
                "x": 89,
                "y": 58
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "4fabce46-06bf-42c2-b9af-cbade8dd0b8b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 11,
                "offset": 0,
                "shift": 7,
                "w": 4,
                "x": 39,
                "y": 58
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "5b4f2383-079d-451d-b29b-49c04a98dd0d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 9,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 20,
                "y": 58
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "7d031d5b-a18a-4c61-8d08-034c2890e376",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 11,
                "offset": 1,
                "shift": 7,
                "w": 4,
                "x": 33,
                "y": 58
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "5a72426b-176f-4d45-99d1-89afb3891d19",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 11,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 44,
                "y": 43
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "d7ccc963-f6cb-4c81-ba73-8ccad68e27c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 12,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 61,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "1d559b51-1e30-460a-92c3-39a24e10cef3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 11,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 51,
                "y": 17
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "4ea75005-2736-46db-9020-ee6b31ae625d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 11,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 44,
                "y": 17
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "cc2158df-44c0-454b-8d73-e518e9cce16c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 11,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 86,
                "y": 17
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "40a25d18-4473-489b-b072-2645d9bef92d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 11,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 58,
                "y": 43
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "afc6e050-9d95-459d-b8e2-26ff2403d3d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 11,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 68,
                "y": 2
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "7e06adce-57cc-430d-8bdf-7412f0982f81",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 11,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 117,
                "y": 2
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "7b6c80b9-18e0-4d6c-9d0c-1da625267531",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 11,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 23,
                "y": 17
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "a0ee5755-f405-480b-ab2e-d5c88382b8ad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 11,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 65,
                "y": 30
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "d3a9bff1-395e-4c00-8757-694f6a8b566a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 11,
                "offset": 1,
                "shift": 7,
                "w": 3,
                "x": 59,
                "y": 58
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "586e5cd4-5b25-4a21-aeca-b3829cff0ba2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 11,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 9,
                "y": 30
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "ab5151ac-86e9-4eb3-bebc-82a37d4828de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 11,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 37,
                "y": 30
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "5eb2141f-27b3-44e5-a70e-dea0ffdd80ba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 11,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 16,
                "y": 43
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "e8fa5bc2-c26e-4680-8600-04b6d819cbaf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 11,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 100,
                "y": 30
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "aff6e50a-7d54-4083-ae54-53eebe5d1e05",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 11,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 103,
                "y": 2
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "556842c5-969b-49ae-83e4-ecbd9db7dc9e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 11,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 51,
                "y": 30
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "07bc83d2-1ecc-4f97-b65f-24b5b119bff1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 11,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 30,
                "y": 30
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "e47c4fa6-e849-40c4-a64f-bd14f587b160",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 12,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 47,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "52534a51-1c4c-4f19-ac64-382a5d52adb7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 11,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 23,
                "y": 30
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "a4ccba2e-eece-4c44-9587-ce3c7fc2ac91",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 11,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 75,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "ea42e62c-dab4-4352-81ad-cf7f274c8530",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 11,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 16,
                "y": 30
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "dd39e069-7e60-443a-a29f-fe48c1944b61",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 11,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 79,
                "y": 30
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "74ec846f-0d9e-498c-84e4-112a3d43de0c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 11,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 51,
                "y": 43
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "4d57f7e7-5113-436c-9708-ed4bf6626d69",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 11,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 93,
                "y": 30
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "a339bd79-683a-4148-aa0f-6baf2e217728",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 11,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 82,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "8b0373be-3453-4e1b-abfb-7c618bb1a4f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 11,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 110,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "ab2f3314-1240-4499-acc6-e589f990e081",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 11,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 37,
                "y": 43
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "4e0adac7-d5be-4f48-93cc-6c86e5ce736b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 12,
                "offset": 2,
                "shift": 7,
                "w": 2,
                "x": 97,
                "y": 58
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "e6ac52bc-36f3-4b0a-a741-1c92355654df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 12,
                "offset": 1,
                "shift": 7,
                "w": 4,
                "x": 120,
                "y": 43
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "d7fd36da-358a-499e-94ca-5eea85b45b5f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 12,
                "offset": 2,
                "shift": 7,
                "w": 2,
                "x": 101,
                "y": 58
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "6586e4a5-80d1-464d-830f-6f08f58db4fd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 10,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 113,
                "y": 43
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "20d36d15-9833-49f8-98c8-60cd95ad65da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 11,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 30,
                "y": 17
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "c0289b54-b4f7-4c13-959b-44fa0a3cfab2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 5,
                "offset": 2,
                "shift": 7,
                "w": 2,
                "x": 2,
                "y": 73
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "1fa3bf33-df8b-48dd-b05e-a566071bda67",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 11,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 9,
                "y": 43
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "04d9ba07-606f-4292-9409-6ceb0878572e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 11,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 86,
                "y": 43
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "e3d8f521-ec07-4664-8c8d-5ef4208da89c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 11,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 89,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "8ec2d251-6fec-4bb9-a161-6dee439978f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 11,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 23,
                "y": 43
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "7bad7cb4-6d1d-46c8-8220-9cb996a0b808",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 11,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 93,
                "y": 17
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "bcf7e460-5973-452f-89de-4a6917c59c93",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 11,
                "offset": 1,
                "shift": 7,
                "w": 4,
                "x": 27,
                "y": 58
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "292a4bf5-1f2a-4ebd-94b5-b0151398ea75",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 13,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 40,
                "y": 2
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "e6c9bd50-9214-4f6b-a572-270f7c6eaed8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 11,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 16,
                "y": 17
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "aad670d9-23f9-4391-a2ce-baaf01f51edb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 11,
                "offset": 1,
                "shift": 7,
                "w": 3,
                "x": 69,
                "y": 58
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "346711e7-5355-47dd-a0fb-819ec3e1190b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 13,
                "offset": 0,
                "shift": 7,
                "w": 4,
                "x": 93,
                "y": 43
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "92aefec1-7926-4051-93ac-e9116ea2e7aa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 11,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 79,
                "y": 43
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "2cb7f2b4-17aa-44b9-8a64-33c8a7a8a6b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 11,
                "offset": 1,
                "shift": 7,
                "w": 3,
                "x": 64,
                "y": 58
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "fedf610e-59bb-4989-affa-c37c2e6f4544",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 11,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 100,
                "y": 17
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "2a90611d-deee-46c8-86f8-49943af804ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 11,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 58,
                "y": 17
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "763075b2-2317-40b5-b350-2e25271378b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 11,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 9,
                "y": 17
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "d0dc0d42-a2cf-45bb-8658-e2239a4a566e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 13,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 26,
                "y": 2
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "d9259c4c-71ae-4118-af07-0607ff1982f2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 13,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 33,
                "y": 2
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "e48f4798-d8ab-488d-a617-d86798434303",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 11,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 72,
                "y": 43
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "1d9cec82-3ef7-4fc8-8702-735612ba14d7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 11,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 107,
                "y": 30
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "c5169fac-9a73-4e3f-b842-d9590470a22a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 11,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 65,
                "y": 17
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "bf93267f-1482-4674-802a-aa551d77067f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 11,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 65,
                "y": 43
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "6ede9b09-5b8d-43fd-81e5-dafe205a28a8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 11,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 107,
                "y": 17
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "dc61f636-a8a3-4ac1-9954-c517998fb436",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 11,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 72,
                "y": 17
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "81ac2482-f6c5-4cca-aa72-66c4a3a5c35e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 11,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 2,
                "y": 17
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "353e896d-c5ed-448a-967a-46f886860e2b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 13,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 19,
                "y": 2
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "98209cf9-b0aa-46ed-a8ba-473d6b4f813a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 11,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 72,
                "y": 30
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "81629b73-67d3-49bc-91b5-d6c416d269d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 12,
                "offset": 1,
                "shift": 7,
                "w": 4,
                "x": 2,
                "y": 58
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "30f8d20e-3f84-476a-8b45-1c8752e20357",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 12,
                "offset": 2,
                "shift": 7,
                "w": 1,
                "x": 114,
                "y": 58
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "b2c73122-75b6-480a-8f7e-32549ce9dfe1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 12,
                "offset": 1,
                "shift": 7,
                "w": 4,
                "x": 8,
                "y": 58
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "1402e8a1-302e-4fd0-a504-b523ecf4de88",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 52,
                "y": 58
            }
        },
        {
            "Key": 127,
            "Value": {
                "id": "5a3b5e27-5a1d-4be7-908a-b63f6a6bfae4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 127,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 45,
                "y": 58
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000a\\u000aDefault Character(9647) ▯",
    "size": 6,
    "styleName": "",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}