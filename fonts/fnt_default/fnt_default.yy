{
    "id": "ba1f8d34-6fa5-414d-acf1-d31329cb2a89",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_default",
    "AntiAlias": 0,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 1,
    "first": 0,
    "fontName": "Arial",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "dece048a-869a-4f1e-8dff-3302d1376460",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "afdb2cb1-4d07-4d8d-9278-739c41760af3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 15,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 95,
                "y": 70
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "9ed8665e-5309-49b5-a3fa-9d68391c3c49",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 15,
                "offset": 1,
                "shift": 5,
                "w": 4,
                "x": 89,
                "y": 70
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "5401db6f-e521-45fd-bf7b-37554833516c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 79,
                "y": 70
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "c34bb693-f29f-4de4-92f6-d6299effefb7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 70,
                "y": 70
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "3c901c4a-4bdc-42ee-99d5-6b341d2f5e34",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 15,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 57,
                "y": 70
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "af41437a-a8cc-4904-ac00-4aeace54714a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 46,
                "y": 70
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "f4d65e0b-0935-4a2b-a1fa-a89e61cb5e1c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 15,
                "offset": 1,
                "shift": 2,
                "w": 2,
                "x": 42,
                "y": 70
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "743ad751-908e-47c6-af4d-71aaf2bade5d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 15,
                "offset": 1,
                "shift": 4,
                "w": 4,
                "x": 36,
                "y": 70
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "aadc2e52-309f-4139-b89a-185710e64cae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 15,
                "offset": 1,
                "shift": 4,
                "w": 4,
                "x": 30,
                "y": 70
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "36945089-41d0-4414-b73f-7a6b09fe28aa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 15,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 99,
                "y": 70
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "6ab78b5d-451f-4144-8cd9-742a4286f90c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 15,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 21,
                "y": 70
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "ee7d0b6e-0dd2-4732-9f89-beb86359efba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 15,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 8,
                "y": 70
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "8806bdf2-6c90-4591-9b5c-3905f57b0c9d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 2,
                "y": 70
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "73c6709e-0b14-40dc-8830-33c978b2a081",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 15,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 121,
                "y": 53
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "57b848df-5e12-4a2d-9ea1-171972de5d57",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 115,
                "y": 53
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "e0128de2-63fa-47cf-8587-ab6e026a3d33",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 15,
                "offset": 1,
                "shift": 7,
                "w": 7,
                "x": 106,
                "y": 53
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "7d378d56-b0bb-469f-bd24-7203c41f1f55",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 15,
                "offset": 1,
                "shift": 7,
                "w": 4,
                "x": 100,
                "y": 53
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "50972d6a-02cb-4422-af7b-af33f61c1668",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 91,
                "y": 53
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "b87878e2-feab-4efb-ac14-d8e1343f434b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 15,
                "offset": 1,
                "shift": 7,
                "w": 7,
                "x": 82,
                "y": 53
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "52eae92f-94fa-420b-841e-0263bf7e56f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 73,
                "y": 53
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "dcf51022-f6a8-4945-9cc0-0d48c2d42a97",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 15,
                "offset": 1,
                "shift": 7,
                "w": 7,
                "x": 12,
                "y": 70
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "d3a27c5e-85a7-4954-8542-f645e905cb89",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 117,
                "y": 70
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "47b7bef9-c93b-4656-b6c2-ac07611e1f0d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 15,
                "offset": 1,
                "shift": 7,
                "w": 7,
                "x": 93,
                "y": 87
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "bacde964-52f8-4d84-b98a-0735338cbbde",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 15,
                "offset": 1,
                "shift": 7,
                "w": 7,
                "x": 2,
                "y": 87
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "5bba7d77-149d-4b59-be6b-aff62e6a0075",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 15,
                "offset": 1,
                "shift": 7,
                "w": 7,
                "x": 61,
                "y": 104
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "f2be3783-fe41-44c2-924a-e6c9ad75c064",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 15,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 57,
                "y": 104
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "930eabe8-c09a-433b-b07e-2129a56749bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 15,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 53,
                "y": 104
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "2a86cabb-001b-4932-9997-088da03196eb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 15,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 44,
                "y": 104
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "75fd5868-ac39-4dbd-8cae-83ae293c2c4f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 15,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 35,
                "y": 104
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "d36a616d-1509-4648-a8c6-3abd73a1c526",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 15,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 26,
                "y": 104
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "0300a3ec-33dc-40ed-8389-e0a3cd639e2d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 15,
                "offset": 1,
                "shift": 7,
                "w": 7,
                "x": 17,
                "y": 104
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "c43b90bb-b61c-40bb-b306-35a801acd86f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 15,
                "offset": 1,
                "shift": 13,
                "w": 13,
                "x": 2,
                "y": 104
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "43e9eb44-739f-4595-9e5a-d955a2a03321",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 113,
                "y": 87
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "d8ff0239-31f7-46c2-87fa-c63adaaffdf0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 70,
                "y": 104
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "a6aa4e32-f541-44f2-95aa-7cfe829d0798",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 102,
                "y": 87
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "bc497118-f148-4b1e-904d-f47aa71abbc1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 83,
                "y": 87
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "c58b15e9-c60c-4720-aa6d-527c5452c8e1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 74,
                "y": 87
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "f1105946-6309-420b-8ee5-cdc287232ae8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 15,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 65,
                "y": 87
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "0a707023-0c2f-4d4d-9801-858731b0f821",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 15,
                "offset": 1,
                "shift": 10,
                "w": 10,
                "x": 53,
                "y": 87
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "5d8c84da-62a7-4459-a5b8-385a432fa60e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 43,
                "y": 87
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "223ba93d-8410-447e-8688-71b4fae00bcc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 15,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 39,
                "y": 87
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "54fb8525-dc1e-4101-affa-0e2be2882df5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 31,
                "y": 87
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "33268f47-2b7a-4eb7-820c-38256919b126",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 20,
                "y": 87
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "f96db56d-6492-4f4a-aeb9-5eb8493869d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 15,
                "offset": 1,
                "shift": 7,
                "w": 7,
                "x": 11,
                "y": 87
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "6cb28a24-38d0-4b80-a512-ccc26ec9e601",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 15,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 61,
                "y": 53
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "38369685-d46f-4962-9440-4c2d1ebb2433",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 106,
                "y": 70
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "af3480a5-9d3b-4117-9289-0db4d4d9ff0a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 15,
                "offset": 1,
                "shift": 10,
                "w": 10,
                "x": 49,
                "y": 53
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "cacfad17-299c-44a2-9ac5-ed41d738bcf7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 91,
                "y": 19
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "15585379-ccb7-49d9-97d0-818e5790084c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 15,
                "offset": 1,
                "shift": 10,
                "w": 10,
                "x": 69,
                "y": 19
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "784af6b4-570b-4399-a221-449f8a47dbf7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 58,
                "y": 19
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "9c883f90-5e5a-4fc5-8c04-03ef3505e256",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 48,
                "y": 19
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "9e5ad538-70cc-44af-a32d-2b2893d55ba8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 38,
                "y": 19
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "53d7620a-25fc-4ae9-8645-0eb45b45e481",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 28,
                "y": 19
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "448c4062-cde2-4baf-ba9a-8366f27ffd8a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 17,
                "y": 19
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "a8f0ec68-91b2-45e9-86a0-51022d8b759d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 15,
                "offset": 0,
                "shift": 12,
                "w": 13,
                "x": 2,
                "y": 19
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "08ebac98-b60c-45a0-8ee4-4e5a121d6098",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 105,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "2b660d45-f1f6-48b1-a859-616f2bb5bb07",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 94,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "e37411d8-5161-458b-b432-29a757cedd03",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 81,
                "y": 19
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "0e5b0128-8ae3-43b3-b691-cc010419836c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 15,
                "offset": 1,
                "shift": 4,
                "w": 4,
                "x": 88,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "7a48c35f-c8e4-41a8-ae2d-8c6ae3249b0b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 73,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "e931da9e-c25e-49cd-8ba6-f51007057763",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 68,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "584b8dce-56c5-4652-97c0-c0a97b638fd2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 15,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 60,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "7c828c44-c75d-4683-8945-7800e977d9d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 9,
                "x": 49,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "510b31af-b772-4b1f-924f-9eccb2245600",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 15,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 44,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "b4d33877-4eb2-4e50-bb14-e3801453453e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 35,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "9c40cdda-4f04-49eb-8355-a3fd11f0e9bf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 15,
                "offset": 1,
                "shift": 7,
                "w": 7,
                "x": 26,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "238937c6-e60f-440b-99cc-e43fab9ba0c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 15,
                "offset": 1,
                "shift": 7,
                "w": 7,
                "x": 17,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "76721355-f621-466b-9e94-5f93701b1fcb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 8,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "b905cfc0-c982-433c-9bbb-b10735ed7d9b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 79,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "203fb1a7-27bb-48f3-b0dd-dec9173cf0bd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 101,
                "y": 19
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "d6cd5c69-3c20-451b-988c-23bdc515c2a8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 72,
                "y": 36
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "bac96965-0d05-4af5-b964-9cb2b3724789",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 15,
                "offset": 1,
                "shift": 7,
                "w": 7,
                "x": 108,
                "y": 19
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "9e0476e7-96ad-4a01-9b9a-210e160b0db4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 15,
                "offset": 1,
                "shift": 3,
                "w": 2,
                "x": 32,
                "y": 53
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "d8bfbae8-8fb2-467f-810a-9bbead512640",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 15,
                "offset": -1,
                "shift": 3,
                "w": 3,
                "x": 27,
                "y": 53
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "c1efd22c-c1f8-42f9-b862-362e1d53336d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 15,
                "offset": 1,
                "shift": 7,
                "w": 7,
                "x": 18,
                "y": 53
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "3db3cfa6-b820-4a75-9c4d-c4cd88f6cd5d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 15,
                "offset": 1,
                "shift": 3,
                "w": 2,
                "x": 14,
                "y": 53
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "698a21c6-4e9b-4ab5-a3df-093d3ce0f775",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 15,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 2,
                "y": 53
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "cae97d5f-9a0b-40d5-adca-cc64962cb8df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 15,
                "offset": 1,
                "shift": 7,
                "w": 7,
                "x": 116,
                "y": 36
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "791b8b4a-4edc-4623-9d56-77ae3bb3ce54",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 107,
                "y": 36
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "93e7459a-d1e3-469d-9a50-80fda51dd647",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 15,
                "offset": 1,
                "shift": 7,
                "w": 7,
                "x": 98,
                "y": 36
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "8ca2e810-3618-40b3-9527-c4fc50e0a359",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 89,
                "y": 36
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "ea6e231b-7275-45ff-b3db-65caf526695a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 15,
                "offset": 1,
                "shift": 4,
                "w": 5,
                "x": 36,
                "y": 53
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "251f7a25-9285-492f-9e9e-d68e85eef5f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 81,
                "y": 36
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "c59c7c07-6fca-4e84-845b-5e2bac5cbdec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 66,
                "y": 36
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "3bb6f40a-608f-48c6-bcef-35e1399c4ef1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 15,
                "offset": 1,
                "shift": 7,
                "w": 7,
                "x": 57,
                "y": 36
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "e656b533-3470-46e9-a410-07b48e5af60e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 48,
                "y": 36
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "ab16bbf9-a816-4bf9-9fa9-865ab4b36bf7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 36,
                "y": 36
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "55a3dd42-54eb-4d5f-8fbd-25ee68801da8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 27,
                "y": 36
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "e922d275-4d2b-49d3-bf41-825a96732a72",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 18,
                "y": 36
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "e3cb12ba-b2b4-4d1a-9055-6b54e7e6edec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 9,
                "y": 36
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "3046a9c1-16e3-4fdc-bb64-a9220fb3b1d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 2,
                "y": 36
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "a0606b3d-3c9c-449e-8310-2d40b59fa3e1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 15,
                "offset": 1,
                "shift": 3,
                "w": 2,
                "x": 117,
                "y": 19
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "1f66ba74-a38e-4572-aa54-f97c7f2e7cde",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 43,
                "y": 53
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "c2a4d522-5103-4f1e-bea4-50f4a02aaddd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 15,
                "offset": 1,
                "shift": 8,
                "w": 8,
                "x": 80,
                "y": 104
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        {
            "id": "3fe5fd83-a8d5-48e9-9ccf-477625ddac68",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 49
        },
        {
            "id": "cceba09d-f3d9-43f5-a597-a03edc65f911",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 84
        },
        {
            "id": "5b712d41-731d-4739-a252-e9fab466d963",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 86
        },
        {
            "id": "2b1f4f73-9631-4feb-95bd-3fa24552b8d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 89
        },
        {
            "id": "eace6b24-d882-415b-9f4d-73109227f7ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 8217
        },
        {
            "id": "c98bf6a1-e835-4f3a-8938-0f1dfb85782c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 44
        },
        {
            "id": "f64a81e1-decb-4a36-b86d-8e89c11a88cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 46
        },
        {
            "id": "129ad1b0-b18e-4672-8762-a5e999d7748c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 84
        },
        {
            "id": "541adebe-d5b2-4809-a99d-d818b2bd50a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 86
        },
        {
            "id": "99c810b7-4455-420e-9c31-2e9837009f19",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 87
        },
        {
            "id": "e9261056-1ac2-4c00-b039-526f3859f2e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 89
        },
        {
            "id": "6fdc4b2a-cc19-48d0-9eb4-3a9826ef4ce2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 44
        },
        {
            "id": "b4089152-2946-41b8-b1cb-e862d82caedd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 46
        },
        {
            "id": "c3c19301-1180-4633-9123-9834f8cfc67d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 65
        },
        {
            "id": "328191ad-8ab5-4bf8-8483-95749aa62b62",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 44
        },
        {
            "id": "986d7a83-faf3-4f86-8fef-458cf608b705",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 46
        },
        {
            "id": "b9e72767-9255-4406-b22b-ff9af7fce804",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 58
        },
        {
            "id": "e11cd31f-f32b-4838-a71a-c5467d08deae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 59
        },
        {
            "id": "5902de9b-2368-45b7-80ec-d12cc38d8155",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 65
        },
        {
            "id": "21975bd3-b7a6-4983-8d03-34c893651ae6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 97
        },
        {
            "id": "fb686293-3fc0-4b42-b13f-03eca2487e01",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 99
        },
        {
            "id": "a93bd6f8-c138-4758-a88c-15f8c620849f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 101
        },
        {
            "id": "232d9705-787a-4e32-a70f-455eae6f98d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 111
        },
        {
            "id": "bbbbdc1b-9438-4390-97b1-b0ede03e5e19",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 115
        },
        {
            "id": "49f40016-1031-4eec-bacd-614903477e02",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 894
        },
        {
            "id": "40dc3d96-7350-4337-b445-79b6c389258c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 44
        },
        {
            "id": "4b118fb0-78ed-419c-917d-59670e14ee8e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 46
        },
        {
            "id": "3dcf1aff-7b62-4323-8ac9-ddab098bc3a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 65
        },
        {
            "id": "f854944b-a682-4af5-9cd6-07989bf5526b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 97
        },
        {
            "id": "d3481e21-6352-49d5-a83d-1ef1ecd45a1f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 44
        },
        {
            "id": "70850d16-e869-41e2-927b-47ab4cf26f24",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 45
        },
        {
            "id": "794e6951-6cf0-4353-837d-f7707bb8467d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 46
        },
        {
            "id": "19a80d94-a959-44f8-9147-a6b3d0f54ae0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 65
        },
        {
            "id": "8318cd40-bc47-44f0-add0-0493e02970a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 97
        },
        {
            "id": "5b5367ba-5f4a-4a08-bcf9-d2d911345ea6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 101
        },
        {
            "id": "bb1628a1-d0fc-4806-9c6e-bef6db5aa2b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 111
        },
        {
            "id": "0080896e-ce9f-444b-ac8e-f0461f197c3a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 112
        },
        {
            "id": "d5698f28-d582-4f37-a4e5-89e9fcb2cdb4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 113
        },
        {
            "id": "3424b8a5-2fd2-4069-ac91-5099341e5e4a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 173
        },
        {
            "id": "c92dc7c5-1dc7-431f-ab00-ca60b004f602",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 44
        },
        {
            "id": "d2279eac-b694-46f5-b100-5c4881c54b3e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 46
        },
        {
            "id": "5d0f51d4-ea8d-4d63-b30d-23acf6840957",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 44
        },
        {
            "id": "92a78154-5607-4418-9309-86f833f12744",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 46
        }
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000a\\u000aDefault Character(9647) ▯",
    "size": 10,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}