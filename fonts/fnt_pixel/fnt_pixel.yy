{
    "id": "ac55f690-2ec8-49de-9cbd-f3411ddf6373",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_pixel",
    "AntiAlias": 0,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 1,
    "first": 0,
    "fontName": "copy 08_56",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "3604986b-2478-4cec-af25-0f1b6790db7a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 13,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 84,
                "y": 29
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "f0db80a4-540d-43ac-ae4d-50526c7e60d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 11,
                "offset": 1,
                "shift": 4,
                "w": 1,
                "x": 84,
                "y": 58
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "8289a720-59fd-49b2-9f50-01048066de4f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 5,
                "offset": 0,
                "shift": 5,
                "w": 3,
                "x": 73,
                "y": 58
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "26879894-470f-44a9-94af-a422a624a4f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 10,
                "offset": 0,
                "shift": 9,
                "w": 7,
                "x": 67,
                "y": 2
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "c06adf62-2109-4344-8818-c61fce6b231b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 12,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 76,
                "y": 2
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "01b2e0fc-2991-4f67-ac53-78b0d2d6f7c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 11,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 13,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "473dad64-0ab4-46cb-9e50-e9eaf4b24bb4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 11,
                "offset": 0,
                "shift": 9,
                "w": 7,
                "x": 41,
                "y": 2
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "6487ffa7-3f84-4279-b2e0-eb48c1295fb3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 5,
                "offset": 1,
                "shift": 4,
                "w": 1,
                "x": 103,
                "y": 58
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "42f0f2f0-7ff0-404f-824b-d937ad80d0e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 12,
                "offset": 0,
                "shift": 5,
                "w": 3,
                "x": 117,
                "y": 44
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "5907bf28-8c99-4fb2-86fa-f77069089a65",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 12,
                "offset": 0,
                "shift": 5,
                "w": 3,
                "x": 112,
                "y": 44
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "153f5670-be9b-4d32-a5b9-b0aeaf1566c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 8,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 105,
                "y": 44
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "4ced5d67-34c1-494a-b23b-0ad7a6f54d31",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 10,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 102,
                "y": 29
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "8699265f-8d1d-45f6-838b-2b86ba38b91e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 13,
                "offset": 0,
                "shift": 4,
                "w": 2,
                "x": 49,
                "y": 58
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "00ccbc1d-24c2-406a-a319-91432392b9c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 8,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 36,
                "y": 58
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "629fb36b-cc3b-464f-8f8e-fa9fa6eab053",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 11,
                "offset": 0,
                "shift": 3,
                "w": 1,
                "x": 90,
                "y": 58
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "e82a016e-7d34-40c5-bdcd-08225fa0076c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 11,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 121,
                "y": 29
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "7f68ca71-819f-4509-b850-0af0f13e317d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 11,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 30,
                "y": 29
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "e20cfc08-fc62-4fd4-8c3b-2a8f5e94f88e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 11,
                "offset": 0,
                "shift": 4,
                "w": 2,
                "x": 69,
                "y": 58
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "fbb1175d-d289-49ca-8e79-eeb891e5b3cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 11,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 44,
                "y": 29
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "899b1926-b850-4939-a25c-5259a7a9c01a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 11,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 44,
                "y": 16
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "b49801ff-d2df-45cf-b549-ea606e316777",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 11,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 79,
                "y": 16
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "85b2d68b-6a0e-4075-a213-88fa92ec6d1b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 11,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 86,
                "y": 16
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "c903a453-73c7-487c-9396-374766ef8ad0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 11,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 58,
                "y": 16
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "32c3ad52-cb15-4a01-b6e4-051cef7a733a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 11,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 30,
                "y": 16
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "e0a51a8d-1029-4445-ab3e-1f00504d7096",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 11,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 65,
                "y": 29
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "b94cffbd-9ab1-49e9-92fe-3aea0a17261c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 11,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 23,
                "y": 29
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "735bff94-e401-4ff2-a981-d3c120e3f685",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 10,
                "offset": 1,
                "shift": 4,
                "w": 1,
                "x": 96,
                "y": 58
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "d3e61bba-c696-4a5f-9fd0-e61db017faee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 13,
                "offset": 0,
                "shift": 4,
                "w": 2,
                "x": 53,
                "y": 58
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "3fc8b792-0065-403a-8d63-d22b136ca05d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 10,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 86,
                "y": 44
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "d80b5d13-b958-4333-9097-167d0a781641",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 8,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 92,
                "y": 44
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "41e431db-dafd-4a45-b9de-3b6f60ae8955",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 10,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 99,
                "y": 44
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "90ce5ab9-1616-4033-bc45-adb15c567225",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 11,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 90,
                "y": 2
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "15fe2fc5-d565-4aa2-ac0c-fa768d5d6ed3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 12,
                "offset": 0,
                "shift": 9,
                "w": 7,
                "x": 23,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "3ae1a84a-3c15-40f4-90d5-ad0b72c30eb4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 11,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 9,
                "y": 16
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "4a6ec5dd-770e-4532-8cd7-af2cb69fa7e2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 11,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 51,
                "y": 16
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "eb8d5ec4-ee35-40c4-8274-1eb89d32e613",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 11,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 65,
                "y": 16
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "a7f5dbcb-82b0-42c9-baeb-fb788bd67d42",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 11,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 2,
                "y": 16
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "58286c12-80c1-4b13-bc5c-9c2f854e2649",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 11,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 111,
                "y": 2
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "ba4272b3-65a3-4f7e-98a6-24669fdc37f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 11,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 51,
                "y": 29
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "0492ad42-7956-41f5-a8ef-623405215819",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 11,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 100,
                "y": 16
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "5b2f4b89-e29d-4b8a-a8a6-9fc7cb1af8f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 11,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 16,
                "y": 29
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "8786fc18-c61f-48d2-a4da-8a3766424d41",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 11,
                "offset": 0,
                "shift": 3,
                "w": 1,
                "x": 93,
                "y": 58
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "83fa6ede-3a44-4ead-8188-a99c4b3e062d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 11,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 115,
                "y": 29
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "38db5164-6b6b-4017-beaf-29bc8a98bc31",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 11,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 58,
                "y": 29
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "40fbbef1-780a-4364-b33d-4e31366a3854",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 11,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 8,
                "y": 44
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "f9493739-677d-4a88-a404-bd4379f7486b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 11,
                "offset": 0,
                "shift": 9,
                "w": 7,
                "x": 50,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "c22b55ce-8aea-488d-92bb-68c21255ac0e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 11,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 23,
                "y": 16
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "d59f9788-c2b6-4276-91e7-a7992b19ae00",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 11,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 2,
                "y": 29
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "c57cf883-d450-4b8d-84ab-eaf3515fba61",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 11,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 9,
                "y": 29
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "7d1ab408-0def-43ee-b62b-9b8461423b16",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 12,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 83,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "f4d1066d-9a2e-4791-af1b-f363990f4912",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 11,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 114,
                "y": 16
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "be0818b9-cda9-472d-a78d-2fedcfa14d5b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 11,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 104,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "0bfabbc2-4cec-4672-a8a7-1a9a1d14bd73",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 11,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 37,
                "y": 16
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "fdecd916-2a2d-4df7-8812-5c39c62c8b16",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 11,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 37,
                "y": 29
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "c86f042b-505a-444f-bc3e-70ce73a44330",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 11,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 118,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "c229cc69-b772-4276-bfc9-38e6d3fd7c29",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 11,
                "offset": 0,
                "shift": 11,
                "w": 9,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "4fab2e1c-9c86-49fe-88f0-bf386fa12168",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 11,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 72,
                "y": 16
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "452c6356-7f1f-4158-886f-f346e6cd8dab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 11,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 97,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "7e60e2fa-0abf-4697-a9ab-74bc260797d0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 11,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 20,
                "y": 44
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "2ab5c45c-409b-4c42-afc8-d99e071200b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 12,
                "offset": 0,
                "shift": 4,
                "w": 2,
                "x": 61,
                "y": 58
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "d2ec36f3-e2ce-47e4-a3fc-5e1d066aa426",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 11,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 107,
                "y": 16
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "e4c0ee15-86cb-4281-b58b-dd88a2179c2f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 12,
                "offset": 0,
                "shift": 4,
                "w": 2,
                "x": 65,
                "y": 58
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "f4779c51-b0a6-4de8-8dd0-410364be9a96",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 6,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 42,
                "y": 58
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "7594f684-4e6e-4c86-ad57-dea30b856365",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 12,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 59,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "ba4c4ffb-92d0-4d9c-8ae2-9c316e4a975a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 4,
                "offset": 0,
                "shift": 4,
                "w": 2,
                "x": 99,
                "y": 58
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "a9f32124-ee57-4d02-9276-ac67cb79aa17",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 11,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 74,
                "y": 44
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "a31da52a-fe5e-4fed-9ebf-d8cf3b378ac6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 11,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 44,
                "y": 44
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "3f9b2792-cac7-4fc2-8918-fcbf780087d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 11,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 62,
                "y": 44
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "4df570c1-8084-4ab8-ac34-0b439d9cf4de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 11,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 14,
                "y": 44
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "d1c86691-1c42-454b-bf1e-07cef4050c9f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 11,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 68,
                "y": 44
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "f23811d0-b89b-48d8-9550-b0ea8451f106",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 11,
                "offset": 0,
                "shift": 5,
                "w": 3,
                "x": 26,
                "y": 58
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "6d66145d-4a76-4fa6-aecf-7c371fe9e99a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 78,
                "y": 29
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "94fd03a7-2cd1-4bb4-9164-b89efbfe1170",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 11,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 56,
                "y": 44
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "fd52d9b2-2697-4dc0-a68c-f0432d3cb5c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 11,
                "offset": 0,
                "shift": 3,
                "w": 1,
                "x": 87,
                "y": 58
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "d1f3455c-8cee-4a05-8830-ca5cdd93451e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 13,
                "offset": 0,
                "shift": 4,
                "w": 2,
                "x": 57,
                "y": 58
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "ef818497-9367-4580-9aa4-b257aaece3da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 11,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 2,
                "y": 44
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "59d0c203-3d99-4a9c-a1f9-c1c995644d0d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 11,
                "offset": 0,
                "shift": 3,
                "w": 1,
                "x": 81,
                "y": 58
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "b6d32b36-28b8-41d9-a814-d3acf89088e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 11,
                "offset": 0,
                "shift": 9,
                "w": 7,
                "x": 32,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "b4f22403-f82c-4a65-a54e-6d1fc501df33",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 11,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 109,
                "y": 29
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "e0cadb32-5e92-476b-ad21-121b66567b5d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 11,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 50,
                "y": 44
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "15a54a23-d2e5-4c8e-b93b-67798565257a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 96,
                "y": 29
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "7873d8bd-657e-4761-b096-e8cb8033b462",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 72,
                "y": 29
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "9e21e55e-b803-4e23-97d0-abe1e2325f08",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 11,
                "offset": 0,
                "shift": 5,
                "w": 3,
                "x": 21,
                "y": 58
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "a0bfe193-96d9-4da5-b041-cc869dd93519",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 11,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 32,
                "y": 44
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "358550bd-0ddd-445a-8b88-10daeacbf231",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 11,
                "offset": 0,
                "shift": 5,
                "w": 3,
                "x": 31,
                "y": 58
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "057771e9-3d2e-4816-a571-827698cb367e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 11,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 38,
                "y": 44
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "d5f417db-aa16-47c7-bb10-c17bc65ec1c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 11,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 93,
                "y": 16
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "27304974-5650-4e15-bf38-ccfc4509cc8c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 11,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 16,
                "y": 16
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "8a67fa7c-9e22-4ce4-993b-6f18e3c6cb82",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 11,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 80,
                "y": 44
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "f6fe703d-b28a-421b-b5d4-e35a4aa426c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 90,
                "y": 29
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "db5ce47d-8863-41d1-af34-24e461a6089e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 11,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 26,
                "y": 44
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "f902c6ef-d9ae-4f48-85c6-52f95b8a6880",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 12,
                "offset": 0,
                "shift": 5,
                "w": 3,
                "x": 2,
                "y": 58
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "f8dfbd02-244e-45f7-9caf-ef4efb0d010f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 12,
                "offset": 1,
                "shift": 4,
                "w": 1,
                "x": 78,
                "y": 58
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "b173e23d-5ba4-4929-8101-636e19a53025",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 12,
                "offset": 0,
                "shift": 5,
                "w": 3,
                "x": 122,
                "y": 44
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "2cba214e-9280-4816-a1eb-6c070af960fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 14,
                "y": 58
            }
        },
        {
            "Key": 127,
            "Value": {
                "id": "c982a8fe-ebe9-4983-80ba-18a309c5bb28",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 127,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 7,
                "y": 58
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000a\\u000aDefault Character(9647) ▯",
    "size": 6,
    "styleName": "",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}