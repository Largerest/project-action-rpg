{
    "id": "764da3a8-0402-4780-aaff-af19b271066c",
    "modelName": "GMPath",
    "mvc": "1.0",
    "name": "pth_ferrosia",
    "closed": true,
    "hsnap": 16,
    "kind": 0,
    "points": [
        {
            "id": "24b70e76-f60e-410d-ada1-93e474962622",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 48,
            "y": 128,
            "speed": 100
        },
        {
            "id": "f3c13ed0-5b11-45cd-86fa-fd17df20307c",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 48,
            "y": 56,
            "speed": 100
        },
        {
            "id": "fb97f109-d9b4-4fe9-b39a-849ddb3c4b7a",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 272,
            "y": 56,
            "speed": 100
        },
        {
            "id": "cecb3c6d-5705-43de-8a27-2007f1bda664",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 256,
            "y": 136,
            "speed": 100
        },
        {
            "id": "25a0697b-c0f4-47f1-81d2-e951d6220742",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 208,
            "y": 136,
            "speed": 100
        },
        {
            "id": "66f8af98-eb37-471e-bccb-0bce0b293b78",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 176,
            "y": 184,
            "speed": 100
        },
        {
            "id": "f3d3a69c-f21c-4f02-b912-e6aa1acf708e",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 112,
            "y": 184,
            "speed": 100
        },
        {
            "id": "49101c80-c600-422d-9320-1a7e9264867b",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 64,
            "y": 128,
            "speed": 100
        }
    ],
    "precision": 4,
    "vsnap": 8
}